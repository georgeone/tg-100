
#include <stdint.h>
#include <string.h>
#include "led_operation.h"
#include "lora_parsing.h"
#include "gps_parsing.h"
#include "storage.h"
#include "dfu_app_handler.h"
#include "app_error.h"
//#include "SEGGER_RTT.h"

// Extern Variable
extern uint32_t lora_cnt;
extern uint8_t lora_eui[8];
extern uint8_t lora_app_key[16];
extern uint8_t packet[46];
extern uint8_t led_state;
extern uint8_t m_beacon_info[23];
extern uint8_t major_minor[8];
extern uint8_t time_schedule[20];
extern uint32_t gps_gathering_time;
extern uint8_t gps_gathering_cycle[4];
extern uint8_t network_mode[4];
extern uint8_t gyro_ignore[4];
extern uint32_t heart_beat_time;
extern uint32_t heart_beat_cycle_time;
extern uint8_t heart_beat_cycle[8];
extern uint8_t provision_fail_cnt;
extern uint8_t set_app_key[16];
extern uint8_t lora_retranse_num[4];

// Flag
extern uint8_t lora_setting_end_flag;
extern uint8_t lora_uart_stop_flag;
extern uint8_t provision_flag;
extern uint8_t provision_success_flag;
extern uint8_t test_lora_flag;
extern uint8_t lora_baudrate_test;
extern uint8_t function_key_flag;
extern uint8_t heart_beat_flag;
extern uint8_t factory_setting_flag;
extern uint8_t lora_no_send_flag;
extern uint8_t device_reset_flag;
extern uint8_t rtc_try_flag;
extern uint8_t gps_valid_flag;
extern uint8_t real_join_flag[4];

// Command Queue
extern char command[80];
extern char command_packet[4][80];
extern uint8_t command_packet_cnt;
extern uint8_t command_packet_front;
extern uint8_t command_packet_rear;

// Extern function
extern void factory_setting_success(void);
extern void mode_op_timer_stop(void);
extern void fix_gps_timer_start(void);
extern void setting_timer_start(void);
extern void advertising_init(void);
extern void uart_stop(void);

// Static Variable
static uint8_t server_command;
static uint8_t reset_command;
static uint8_t save_app_key;
static uint8_t save_retranse_num;
static uint8_t network_mode_change_command;
static uint8_t no_rx_cnt = 0;
static char lora_retranse[11] = "AT+TXRN ";

// Command Enqueue
void command_enqueue(void){
	
	memcpy(&command_packet[command_packet_rear][0],command,sizeof(command));
	command_packet_rear++;
	if( 6 == command_packet_rear ){
		command_packet_rear = 0;
	}
	
	if( command_packet_rear == command_packet_front){
		command_packet_front++;
	}
	
	if( 6 == command_packet_front ){
		command_packet_front = 0;
	}
	
	if( 5 != command_packet_cnt){
		command_packet_cnt++;
	}
}

// Command Dequeue
void command_dequeue(void){
	memset(&command_packet[command_packet_front][0],0,sizeof(command));
	command_packet_front++;
	if( 6 == command_packet_front ){
		command_packet_front = 0;
	}
		
	if( command_packet_front == command_packet_rear){
		command_packet_front = 0;
		command_packet_rear = 0;
	}
	
	if( 0 != command_packet_cnt ){
		command_packet_cnt--;
	}
}

// Application EUI, Device EUI Check
uint8_t eui_check(uint8_t *eui){
	uint8_t check_cnt=0;
	for(int i=0;i<8;i++){
		if(0 == eui[i]){
			check_cnt++;
		}
	}
	if(8 == check_cnt){
		return true;
	}
	else{
		return false;
	}
}

// Application Key Check
uint8_t app_key_check(uint8_t *app_key){
	uint8_t check_cnt=0;
    uint8_t check_ff_cnt=0;
	for(int i=0;i<16;i++){
		if(0 == app_key[i] || 0xff == app_key[i]){
			check_cnt++;
		}
        else if(0xff == app_key[i])
        {
            check_ff_cnt++;
        }
	}
	if(16 == check_cnt || 16 == check_ff_cnt){
		return true;
	}
	else{
		return false;
	}
}

// String to Hex
int string_to_hex(const char *parm_string) 
{ 
    int count = strlen(parm_string), i = 0; 
    int hexa = 0; 
    for(i = 0; i < count; i++){ 
        if(*parm_string >= '0' && *parm_string <= '9') hexa = hexa*16 + *parm_string - '0'; 
        else if(*parm_string >= 'A' && *parm_string <= 'F') hexa = hexa*16 + *parm_string - 'A' + 10; 
        else if(*parm_string >= 'a' && *parm_string <= 'f') hexa = hexa*16 + *parm_string - 'a' + 10; 
        parm_string++; 
    } 
    return hexa; 
} 

void lora_parsing(char *receive_data,uint8_t p_index){
	
	// Device EUI Read
	if(0 == strncmp(&receive_data[1],"Device EUI",strlen("Device EUI"))){
		
		char temp[2];
		int i,j=0;
		
		for(i=0;i<22;i=i+3){
			strncpy(temp,&receive_data[14+i],2);
			lora_eui[j] = string_to_hex(temp);
			j++;
		}

		if(lora_setting_end_flag == 1){
			if(0 != command_packet_cnt){
				nrf_gpio_pin_clear(GPIO_LORA_INTP_PIN);
				setting_timer_start();
			}
			else{
				lora_uart_stop_flag = 1;
			}
		}
		
	}
	
	// Application Key Read
	else if(0 == strncmp(&receive_data[1],"Application Key (AES)",strlen("Application Key (AES)"))){
		
		char temp[2];
		int i,j=0;
		for(i=0;i<46;i=i+3){
			strncpy(temp,&receive_data[25+i],2);
			lora_app_key[j] = string_to_hex(temp);
			j++;
		}
		
		// pstorage에 App Key 저장
		if(1 == save_app_key){
			j=0;
			save_app_key = 0;
			
			for(i=0;i<46;i=i+3){
				strncpy(temp,&receive_data[25+i],2);
				set_app_key[j] = string_to_hex(temp);
				j++;
			}
			
			storage_update(DEFAULT_DATA_BLOCK,set_app_key,16,64);
		}
		
	}
	
	// Set Tx retranse number 
	else if(0 == strncmp(&receive_data[1],"Tx Re-transmission Number",strlen("Tx Re-transmission Number"))){
		
		if(1 == save_retranse_num){
			
			save_retranse_num = 0;
			
			char temp;
			temp = receive_data[29];
			
			memset(lora_retranse_num,0,sizeof(lora_retranse_num));
			
			lora_retranse_num[1] = string_to_hex(&temp);
			
			storage_update(DEFAULT_DATA_BLOCK,lora_retranse_num,4,80);
			
		}
		
	}
	
	// Pseudo Join, Real Join fail message
	else if(0 == strncmp(receive_data,"ERROR-JOIN FAIL",strlen("ERROR-JOIN FAIL"))){
		
		provision_fail_cnt++;

		// AT Command가 있으면 lora set timer 시작
		if(0 != command_packet_cnt && 8 != provision_fail_cnt){
			setting_timer_start();
		}
		
	}
	
	// Rx 받지 못할 때, Tx retranse number 만큼 전송 시도
	else if(0 == strncmp(receive_data,"ERROR-NO RX",strlen("ERROR-NO RX"))){
		
		no_rx_cnt++;
		
		if(lora_retranse_num[1] == no_rx_cnt){
			lora_no_send_flag = 1;
		}
		
		clear_led();
		led_state = 8;
		
	}
	
	// RF 사용 중일때 뜨는 메시지
	else if(0 == strncmp(receive_data,"ERROR-RF BUSY",strlen("ERROR-RF BUSY"))){
		
		clear_led();
		led_state = 8;
		
	}
	
	// 모든 커맨드의 끝, 데이터를 보내고 나서 더 보낼게 있으면 전송
	else if(0 == strncmp(receive_data,"END",strlen("END"))){
		
		// Downlink로 reset을 했을 때
		if(1 == reset_command){
			reset_command = 0;
			uart_stop();
			device_reset();
		}
		
		// Downlink로 네트워크 모드 변경했을 때
		else if(1 == network_mode_change_command){
			device_reset_flag = 1;
		}
		
		// LoRa 설정 단계가 아닐 때
		if(1 == lora_setting_end_flag){
			
			// AT Command가 있으면 lora set timer 시작
			if(0 != command_packet_cnt){
				nrf_gpio_pin_clear(GPIO_LORA_INTP_PIN);
				nrf_gpio_pin_set(GPIO_LORA_INTP_PIN);
				setting_timer_start();
			}
			else{
				no_rx_cnt = 0;
				lora_uart_stop_flag = 1;
			}
			
		}
		
	}
	
	// Pseudo Join Success
	else if(0 == strncmp(receive_data,"OK-JOIN ACCEPT",strlen("OK-JOIN ACCEPT"))){
		
		provision_fail_cnt = 0;
		
	}

	// Rx Success
	else if(0 == strncmp(receive_data,"OK-RECEIVE ACK",strlen("OK-RECEIVE ACK"))){
	
		// Heart Beat는 LoRa 전송이 되는 상황이라면 필요하지 않기 때문에 0으로 변경
		heart_beat_flag = 0;
		// Packet에 SOS Status가 실렸고 전송이 성공하면 function_key_flag와 Packet의 SOS Status 초기화
		if(2 == function_key_flag){
            function_key_flag = 0;
			packet[39] &= ~(0x01 << 2);
		}
		
	}
	
	// Save
	else if(0 == strncmp(receive_data,"OK-SAVE CONFIG",strlen("OK-SAVE CONFIG"))){
		
		// Factory setting 후 디바이스 종료
		if(1 == factory_setting_flag){
            if(command_packet_cnt == 0){
                factory_setting_success();
            }
		}
		// LoRa설정 단계가 아닐 때
		if(1 == lora_setting_end_flag){
			
			// AT Command가 있으면 lora set timer 시작
			if(0 != command_packet_cnt){
				nrf_gpio_pin_clear(GPIO_LORA_INTP_PIN);
				setting_timer_start();
			}
			else{
				lora_uart_stop_flag = 1;
			}
			
		}
		
	}
	
	// Set Application Key
	else if(0 == strncmp(receive_data,"OK-SET PAK",strlen("OK-SET PAK"))){
		
		save_app_key = 1;
		
	}
	
	// Set Tx Retranse number 
	else if(0 == strncmp(receive_data,"OK-SET TXRN",strlen("OK-SET TXRN"))){
		
		save_retranse_num = 1;
		
	}
	
	// Real Join Success
	else if(0 == strncmp(receive_data,"OK-PROVISION ACCEPT",strlen("OK-PROVISION ACCEPT"))){
		
		provision_fail_cnt = 0;
		provision_success_flag = 1;
		provision_flag = 1;
		
		gps_gathering_time = 0;
		
		// Real Join이 됐다면 flag를 0으로 만들어 저장
		if(1 == real_join_flag[1]){
			
			memset(real_join_flag,0,sizeof(real_join_flag));
			
			storage_update(DEFAULT_DATA_BLOCK,real_join_flag,4,92);
			
		}
		
		// RTC 설정이 안 됐다면 rtc_try_flag를 0으로 바꿔 다시 시도하게
		if(0 == gps_valid_flag){
			rtc_try_flag = 0;
		}
		
		clear_led();
		led_state = 0;
		
	}
	
	// LoRa Downlink
	else if(0 == strncmp(receive_data,"-Port",strlen("-Port"))){
		
		if(0 == strncmp(&receive_data[8],"de",strlen("de"))){
			
			server_command = 1;
			
		}
		else if(0 == strncmp(&receive_data[8],"df",strlen("df"))){
			
			server_command = 2;
			
		}
		
	}
	
	else if(0 == strncmp(receive_data,"-Data",strlen("-Data"))){
		
		// -Port : de
		if(1 == server_command){
			
			char s_message_type[2];
			uint8_t message_type;
			
			server_command = 0;
			
			strncpy(s_message_type,&receive_data[11],2);
			message_type = string_to_hex(s_message_type);
			
			// 스파코사 서버 명령
			if( 0x00 <= message_type && 0x7F >= message_type){
				
				// [14] length
				// [17] protocol type
				// [20] data 
				char s_length[2],s_protocol_type[2];
				uint8_t length,protocol_type;
				
				strncpy(s_length,&receive_data[14],2);
				length = string_to_hex(s_length);
				
				strncpy(s_protocol_type,&receive_data[17],2);
				protocol_type = string_to_hex(s_protocol_type);
				
				// Major / Minor 설정
				if(0x05 == length && 0x01 == protocol_type){
					
					char temp[2];
					int i,j=0;
					
					memset(major_minor,0,sizeof(major_minor));

					for(i=0;i<12;i=i+3){
						strncpy(temp,&receive_data[20+i],2);
						major_minor[1+j] = string_to_hex(temp);
						j++;
					}
					
					storage_update(DEFAULT_DATA_BLOCK,major_minor,8,0);
					
					memcpy(&m_beacon_info[18],&major_minor[1],4);
					advertising_init();
					
				}
				
				// 타임스케쥴 변경
				else if(0x14 == length && 0x02 == protocol_type){
					
					char temp[2];
					int i,j=0;
					
					memset(time_schedule,0,sizeof(time_schedule));
					
					for(i=0;i<57;i=i+3){
						strncpy(temp,&receive_data[20+i],2);
						time_schedule[1+j] = string_to_hex(temp);
						j++;
					}
					storage_update(DEFAULT_DATA_BLOCK,time_schedule,20,8);
					
				}
				
				// 위치 수집 주기 변경
				else if(0x02 == length && 0x03 == protocol_type){
					
					if(0 != strncmp(&receive_data[20],"00",strlen("00"))){
						
						char temp[2];
						
						memset(gps_gathering_cycle,0,sizeof(gps_gathering_cycle));
						
						strncpy(temp,&receive_data[20],2);
						gps_gathering_cycle[1] = string_to_hex(temp);
						
						storage_update(DEFAULT_DATA_BLOCK,gps_gathering_cycle,4,28);
						
						gps_gathering_time = 0;

					}
				}
				
				// 네트워크 모드 변경
				else if(0x02 == length && 0x04 == protocol_type){
					
					if(0 == strncmp(&receive_data[20],"00",strlen("00")) || 0 == strncmp(&receive_data[20],"01",strlen("01"))){
						
						char temp[2];
						
						memset(network_mode,0,sizeof(network_mode));
						
						strncpy(temp,&receive_data[20],2);
						network_mode[1] = string_to_hex(temp);	
						
						storage_update(DEFAULT_DATA_BLOCK,network_mode,4,32);
						
						mode_op_timer_stop();
						network_mode_change_command = 1;
					}
				}
				
				// Heart Beat Cycle 변경
				else if(0x05 == length && 0x0C == protocol_type){

					char temp[2];
					int i,j=0;
					
					memset(heart_beat_cycle,0,sizeof(heart_beat_cycle));
					
					for(i=0;i<12;i=i+3){
						strncpy(temp,&receive_data[20+i],2);
						heart_beat_cycle[1+j] = string_to_hex(temp);
						j++;
					}
						
					if(0 == heart_beat_cycle[1] && 0 == heart_beat_cycle[2] && 0 == heart_beat_cycle[3] && 10 > heart_beat_cycle[4]){
						heart_beat_cycle[4] = 10;
					}
				
					memcpy(&heart_beat_cycle_time,&heart_beat_cycle[1],4);
				
					heart_beat_cycle_time = little_to_big32(heart_beat_cycle_time);
				
					storage_update(DEFAULT_DATA_BLOCK,heart_beat_cycle,8,52);
					
					heart_beat_time = 0;
					
				}
				
				// 자이로 센서 On / Off
				else if(0x02 == length && 0x0D == protocol_type){
					
					if(0 == strncmp(&receive_data[20],"00",strlen("00")) || 0 == strncmp(&receive_data[20],"01",strlen("01"))){
						
						char temp[2];
						
						memset(gyro_ignore,0,sizeof(gyro_ignore));
	
						strncpy(temp,&receive_data[20],2);
						gyro_ignore[1] = string_to_hex(temp);
						
						storage_update(DEFAULT_DATA_BLOCK,gyro_ignore,4,84);
					}
					
				}
				
			}
			
			// 리셋 명령
			else if(0x80 == message_type){
				
				reset_command = 1;
				
			}
			
			// LoRa 수집 주기 변경 명령
			else if(0x81 == message_type){
			}
			
			// 즉시보고 명령
			else if(0x82 == message_type){
			}
			
			// SKT RFU 명령
			else{
			}
			
		}
		
		// -Port : df
		else if(2 == server_command){
			
			server_command = 0;
			
			// [11] 재전송 횟수 변경
			// [14] length
			// [17] data , 0일 경우 3회 고정
			if(0 == strncmp(&receive_data[11],"04",strlen("04"))){
				
				if(0 == strncmp(&receive_data[14],"01",strlen("01"))){
					
					if(0 == strncmp(&receive_data[17],"00",strlen("00"))){
						strcat(lora_retranse,"03");
						lora_retranse_num[1] = 3;
					}
					else{
						char temp[2];
						strncpy(temp,&receive_data[17],2);
						strcat(lora_retranse,temp);
						lora_retranse_num[1] = string_to_hex(temp);
					}
					
					memset(command,0,sizeof(command));
					command[0] = 10;
					strcpy(&command[1],lora_retranse);
					command_enqueue();
					memset(&lora_retranse[8],0,sizeof(lora_retranse)-8);
					
					storage_update(DEFAULT_DATA_BLOCK,lora_retranse_num,4,80);

				}
			}
		}
	}
}

void test_lora_parsing(char *receive_data,uint8_t p_index){
	
	if(strncmp(receive_data,"OK",strlen("OK")) == 0){
		
		if(0 == test_lora_flag){
			lora_baudrate_test = 1;
		}
		else{
			m_beacon_info[6] = 1;
			advertising_init();
		}
		
	}
	
}
