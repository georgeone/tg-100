
#include <stdint.h>

void storage_init(int block_size, int block_count);

void storage_load(int ident, uint8_t * dest, int size, int offset);

void storage_save(int ident, uint8_t * source, int size, int offset);

void storage_clear(int block_size, int block_count);

void storage_update(uint16_t ident, uint8_t * source, int size, int offset);


//////////   pstorage block   //////////
//   
//   100byte 1block ( 84byte use )
//   ------------------------------------
//   |   name         |       size      |
//   ------------------------------------
//   |	major / minor |  8byte(4byte)   |
//   |  timeschedule	|	20byte(19byte)  |
//	 |  gps cycle			|  4byte(1byte)   |
//   |  mode					|  4byte(1byte)   |
//   |  adv interval  |  4byte(1byte)		|
//   |  packet header |  8byte(5byte)   |
//	 |  gps fix time  |  4byte(1byte)		|
//   |  heart beat    |  4byte(1byte)   |
//   |  sos clear     |  4byte(1byte)   |
//   |  app key       | 16byte(16byte) 	|
//   |  gyro ignore   |  4byte(1byte)   |
//   |  first boot    |	 4btte(1byte)   |
//   | lora retranse  |  4byte(1byte)   |
//   ------------------------------------


