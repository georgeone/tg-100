/* Copyright (c) 2014 Nordic Semiconductor. All Rights Reserved.
 *
 * The information contained herein is property of Nordic Semiconductor ASA.
 * Terms and conditions of usage are described in detail in NORDIC
 * SEMICONDUCTOR STANDARD SOFTWARE LICENSE AGREEMENT.
 *
 * Licensees are granted free, non-transferable use of the information. NO
 * WARRANTY of ANY KIND is provided. This heading must NOT be removed from
 * the file.
 *
 */

/** @file
 *
 * @defgroup ble_sdk_uart_over_ble_main main.c
 * @{
 * @ingroup  ble_sdk_app_nus_eval
 * @brief    UART over BLE application main file.
 *
 * This file contains the source code for a sample application that uses the Nordic UART service.
 * This application uses the @ref srvlib_conn_params module.
 */

//#define AT_COMMAND
#include <stdint.h>
#include <string.h>
#include "nordic_common.h"
#include "nrf.h"
#include "ble_hci.h"
#include "ble_advdata.h"
#include "ble_conn_params.h"
#include "softdevice_handler.h"
#include "app_timer.h"
#include "ble_nus.h"
#include "app_uart.h"
#include "app_util_platform.h"
#include "pstorage.h"
#include "custom_board.h"
#include "nrf_delay.h"
#include "timestamp.h"
#include "ble_dfu.h"
#include "dfu_app_handler.h"
#include "device_manager.h"
#include "setting_service.h"
#ifdef AT_COMMAND
    #include "lora_service.h"
#endif
#include "nrf_drv_spi.h"
#include "bma250e_spi.h"
#include "ble_bas.h"
#include "storage.h"
#include "batt.h"
#include "led_operation.h"
#include "lora_parsing.h"
#include "gps_parsing.h"
#include "bootloader_util.h"
#include "SEGGER_RTT.h"

///////////////////////////////////////////////////////  DEBUG  ///////////////////////////////////////////////////////////////////////

#define DEBUGGING_MODE

#ifdef DEBUGGING_MODE
    #define debug_print SEGGER_RTT_printf
#else 
    #define debug_print(...)
#endif

///////////////////////////////////////////////////////  DEFINE  //////////////////////////////////////////////////////////////////////

#define IS_SRVC_CHANGED_CHARACT_PRESENT 1                                            /**< Include the service_changed characteristic. If not enabled, the server's database cannot be changed for the lifetime of the device. */

#define CENTRAL_LINK_COUNT              0                                            /**< Number of central links used by the application. When changing this number remember to adjust the RAM settings*/
#define PERIPHERAL_LINK_COUNT           1                                            /**< Number of peripheral links used by the application. When changing this number remember to adjust the RAM settings*/

#define APP_BEACON_TIMEOUT_IN_SECONDS		60
#define APP_BEACON_NON_TIMEOUT					0

#define APP_TIMER_PRESCALER             0                                            /**< Value of the RTC1 PRESCALER register. */
#define APP_TIMER_OP_QUEUE_SIZE         10                                           /**< Size of timer operation queues. */

#define MIN_CONN_INTERVAL               MSEC_TO_UNITS(20, UNIT_1_25_MS)              /**< Minimum acceptable connection interval (20 ms), Connection interval uses 1.25 ms units. */
#define MAX_CONN_INTERVAL               MSEC_TO_UNITS(75, UNIT_1_25_MS)              /**< Maximum acceptable connection interval (75 ms), Connection interval uses 1.25 ms units. */
#define SLAVE_LATENCY                   0                                            /**< Slave latency. */
#define CONN_SUP_TIMEOUT                MSEC_TO_UNITS(4000, UNIT_10_MS)              /**< Connection supervisory timeout (4 seconds), Supervision Timeout uses 10 ms units. */
#define FIRST_CONN_PARAMS_UPDATE_DELAY  APP_TIMER_TICKS(5000, APP_TIMER_PRESCALER)   /**< Time from initiating event (connect or start of notification) to first time sd_ble_gap_conn_param_update is called (5 seconds). */
#define NEXT_CONN_PARAMS_UPDATE_DELAY   APP_TIMER_TICKS(30000, APP_TIMER_PRESCALER)  /**< Time between each call to sd_ble_gap_conn_param_update after the first call (30 seconds). */
#define MAX_CONN_PARAMS_UPDATE_COUNT    3                                            /**< Number of attempts before giving up the connection parameter negotiation. */

#define DEAD_BEEF                       0xDEADBEEF                                   /**< Value used as error code on stack dump, can be used to identify stack location on stack unwind. */

#define UART_TX_BUF_SIZE                256                                          /**< UART TX buffer size. */
#define UART_RX_BUF_SIZE                256                                          /**< UART RX buffer size. */

#define POWER_TIMER_INTERVAL					APP_TIMER_TICKS(100,APP_TIMER_PRESCALER)
#define SETTING_INIT_TIMER_INTERVAL			    APP_TIMER_TICKS(200,APP_TIMER_PRESCALER)
#define RTC_TIMER_INTERVAL						APP_TIMER_TICKS(1000,APP_TIMER_PRESCALER)
#define ACCEL_TIMER_INTERVAL					APP_TIMER_TICKS(100,APP_TIMER_PRESCALER)
#define MODE_OP_TIMER_INTERVAL					APP_TIMER_TICKS(1000,APP_TIMER_PRESCALER)
#define NO_FIX_GPS_TIMER_INTERVAL				APP_TIMER_TICKS(100,APP_TIMER_PRESCALER)
#define FIX_GPS_TIMER_INTERVAL					APP_TIMER_TICKS(100,APP_TIMER_PRESCALER)	
#define LORA_TIMER_INTERVAL						APP_TIMER_TICKS(100,APP_TIMER_PRESCALER)
#define LORA_SET_TIMER_INTERVAL					APP_TIMER_TICKS(200,APP_TIMER_PRESCALER)
#define LORA_ON_TIMER_INTERVAL					APP_TIMER_TICKS(200,APP_TIMER_PRESCALER)
#define TEST_ACCEL_TIMER_INTERVAL				APP_TIMER_TICKS(1000,APP_TIMER_PRESCALER)
#define TEST_INIT_TIMER_INTERVAL				APP_TIMER_TICKS(100,APP_TIMER_PRESCALER)
#define TEST_FIX_TIMER_INTERVAL					APP_TIMER_TICKS(1000,APP_TIMER_PRESCALER)

#define APP_BEACON_INFO_LENGTH          0x17                              					 /**< Total length of information advertised by the Beacon. */
#define APP_ADV_DATA_LENGTH             0x15                              					 /**< Length of manufacturer specific data in the advertisement. */
#define APP_DEVICE_TYPE                 0x02                              				 	 /**< 0x02 refers to Beacon. */
#define APP_MEASURED_RSSI               0xC3                            					   /**< The Beacon's measured RSSI at 1 meter distance in dBm. */
#define APP_COMPANY_IDENTIFIER          0x004C                           						 /**< Company identifier for Nordic Semiconductor ASA. as per www.bluetooth.org. */
#define APP_MAJOR_VALUE                 0x00, 0x01                        					 /**< Major value used to identify Beacons. */ 
#define APP_MINOR_VALUE                 0x00, 0x01                        					 /**< Minor value used to identify Beacons. */ 
#define APP_BEACON_UUID                 0x2e, 0x77, 0x13, 0xc8, \
                                        0x20, 0x9e, 0x11, 0xe6, \
                                        0xbb, 0xa7, 0x40, 0xf2, \
                                        0xe9, 0xcc, 0x23, 0xa2            					 /**< Proprietary UUID for Beacon. 2e7713c8-209e-11e6-bba7-40f2e9cc23a2 */
															
#define DFU_REV_MAJOR                   0x00                                       	 /** DFU Major revision number to be exposed. */
#define DFU_REV_MINOR                   0x01                                       	 /** DFU Minor revision number to be exposed. */
#define DFU_REVISION                    ((DFU_REV_MAJOR << 8) | DFU_REV_MINOR)       /** DFU Revision number to be exposed. Combined of major and minor versions. */
#define APP_SERVICE_HANDLE_START       	0x000C                                       /**< Handle of first application specific service when when service changed characteristic is present. */
#define BLE_HANDLE_MAX                  0xFFFF                                       /**< Max handle value in BLE. */

STATIC_ASSERT(IS_SRVC_CHANGED_CHARACT_PRESENT);																			 /** When having DFU Service support in application the Service Changed Characteristic should always be present. */
																																								
#define SEC_PARAM_BOND                  1                                            /**< Perform bonding. */
#define SEC_PARAM_MITM                  0                                            /**< Man In The Middle protection not required. */
#define SEC_PARAM_LESC                  0                                            /**< LE Secure Connections not enabled. */
#define SEC_PARAM_KEYPRESS              0                                            /**< Keypress notifications not enabled. */
#define SEC_PARAM_IO_CAPABILITIES       BLE_GAP_IO_CAPS_NONE                         /**< No I/O capabilities. */
#define SEC_PARAM_OOB                   0                                            /**< Out Of Band data not available. */
#define SEC_PARAM_MIN_KEY_SIZE          7                                            /**< Minimum encryption key size. */
#define SEC_PARAM_MAX_KEY_SIZE          16                                           /**< Maximum encryption key size. */

#define SET_MAJOR_MINOR							0x01
#define SET_TIME_SCHEDULE						0x02
#define SET_GPS_GATHERING_CYCLE					0x03
#define SET_MODE								0x04
#define SET_ADVERTISING_INTERVAL				0x05
#define SET_PACKET_HEADER						0x06
#define SET_GPS_FIX_TRY_TIME					0x07
#define SET_SOS_CLEAR_TIME						0x0B
#define SET_HEART_BEAT_CYCLE					0x0C
#define SET_GYRO_IGNORE							0x0D

#define GET_MAJOR_MINOR							0x11
#define GET_TIME_SCHEDULE						0x12
#define GET_GPS_GATHERING_CYCLE					0x13
#define GET_MODE								0x14
#define GET_ADVERTISING_INTERVAL				0x15
#define GET_PACKET_HEADER						0x16
#define GET_GPS_FIX_TRY_TIME					0x17
#define GET_LORA_EUI							0x18
#define GET_CURRENT_TIME						0x19
#define GET_FIRMWARE_VERSION					0x1A
#define GET_SOS_CLEAR_TIME						0x1B
#define GET_HEART_BEAT_CYCLE					0x1C
#define GET_GYRO_IGNORE							0x1D
#define GET_LORA_APP_KEY						0x20

#define FACTORY_SETTING							0xff

#define SPI_INSTANCE  							0 	

#define BLOCK_SIZE								100
#define	BLOCK_COUNT								1

#define DEFAULT_DATA_BLOCK						0	

////////////////////////////////////////////////////  VARIABLE  /////////////////////////////////////////////////////////////////////////

// Service Info
static char *DEVICE_NAME 		  = "GPER"; 
static ble_nus_t                  m_nus;                        								/**< Structure to identify the Nordic UART Service. */
static ble_dfu_t                  m_dfus;    																		/**< Structure used to identify the DFU service. */
static ble_bas_t				  m_bas;																				// Battery Service
static ble_setting_t 			  m_setting;              											// Setting Service
#ifdef AT_COMMAND
    static ble_lora_t				m_lora;																				// LoRa Service
#endif
static uint16_t                         m_conn_handle = BLE_CONN_HANDLE_INVALID;    	/**< Handle of the current connection. */
static dm_application_instance_t        m_app_handle;                        					/**< Application identifier allocated by device manager */
static ble_gap_adv_params_t 			m_adv_params;																	// Advertising Parameter
static const nrf_drv_spi_t 				spi = NRF_DRV_SPI_INSTANCE(SPI_INSTANCE);  		/**< SPI instance. */

// Timer Define
APP_TIMER_DEF(power_timer_id);
APP_TIMER_DEF(setting_init_timer_id);
APP_TIMER_DEF(rtc_timer_id);
APP_TIMER_DEF(accel_timer_id);
APP_TIMER_DEF(mode_op_timer_id);
APP_TIMER_DEF(no_fix_gps_timer_id);
APP_TIMER_DEF(fix_gps_timer_id);
APP_TIMER_DEF(lora_timer_id);
APP_TIMER_DEF(lora_set_timer_id);
APP_TIMER_DEF(lora_on_timer_id);
APP_TIMER_DEF(test_accel_timer_id);
APP_TIMER_DEF(test_init_timer_id);
APP_TIMER_DEF(test_fix_timer_id);

// Power, LED State
uint8_t led_state									=	0;
uint8_t power_flag 									= 0;																																																							

// Count
static uint8_t power_cnt 							= 0;
static uint8_t beacon_cnt							= 0;
static uint8_t function_key_cnt						= 0;
static uint8_t setting_init_cnt						= 0;
static uint8_t no_fix_gps_cnt						= 0;
static uint32_t fix_gps_cnt							= 0;
static uint8_t lora_setting_cnt						= 0;
static uint8_t lora_set_cnt							= 0;
static uint8_t lora_on_cnt							= 0;
static uint8_t device_reset_cnt						= 0;
static uint8_t factory_reset_cnt					= 0;
static uint8_t test_init_cnt						= 0;
static uint8_t test_gps_fix_cnt						= 0;
uint32_t lora_cnt									= 0;
uint8_t	provision_fail_cnt							= 0;

// Flag
static uint8_t uart_switch 							= 0;
static uint8_t advertising_flag						= 0;
static uint8_t gps_uart_flag						= 0;
static uint8_t lora_uart_flag						= 0;
static uint8_t gps_setting_flag						= 0;
static uint8_t lora_setting_flag					= 0;
static uint8_t gps_wait_flag						= 0;
static uint8_t is_save_app_key						= 0;
static uint8_t sos_flag								= 0;
uint8_t device_reset_flag							= 0;
uint8_t fixed_uuid_flag								= 0;
uint8_t provision_flag								= 0;
uint8_t provision_success_flag						= 0;
uint8_t gyro_flag									= 0;
uint8_t gps_flag									= 0;
uint8_t lora_setting_end_flag						= 0;
uint8_t function_key_flag							= 0;
uint8_t gps_valid_flag								= 0;
uint8_t no_fix_gps_uart_stop_flag					= 0;
uint8_t fix_gps_uart_stop_flag						= 0;
uint8_t power_off_flag								= 0;
uint8_t factory_setting_flag						= 0;
uint8_t factory_reset_flag							= 0;
uint8_t lora_no_send_flag							= 0;
uint8_t test_lora_flag								= 0;
uint8_t lora_baudrate_test							= 0;
uint8_t gps_no_data_flag							= 0;
uint8_t satellite_flag								= 0;
uint8_t	heart_beat_flag								= 0;
uint8_t test_result_flag							= 0;
uint8_t provision_try_flag 							= 0;
uint8_t rtc_try_flag								= 0;
uint8_t rtc_flag 									= 0;
uint8_t gps_fix_out_flag							= 0;
uint8_t	lora_out_flag								= 0;
uint8_t	lora_uart_stop_flag							= 0;

// Beacon info
uint8_t m_beacon_info[APP_BEACON_INFO_LENGTH] =                    						/**< Information advertised by the Beacon. */
{
    APP_DEVICE_TYPE,     																											// Manufacturer specific information. Specifies the device type in this implementation. 
    APP_ADV_DATA_LENGTH, 																											// Manufacturer specific information. Specifies the length of the manufacturer specific data in this implementation.
    APP_BEACON_UUID,     																											// 128 bit UUID value. 
    APP_MAJOR_VALUE,     																											// Major arbitrary value that can be used to distinguish between Beacons. 
    APP_MINOR_VALUE,     																											// Minor arbitrary value that can be used to distinguish between Beacons. 
    APP_MEASURED_RSSI    																											// Manufacturer specific information. The Beacon's measured TX power in this implementation. 
};

// Second, Time
time_t 	time;

uint32_t		gps_gathering_time						= 0;
uint32_t		heart_beat_time							= 0;
uint32_t		gps_fix_second							= 0;
uint32_t		lora_out_second							= 0;
uint32_t		heart_beat_cycle_time					= 0;
uint16_t 		satellite_wait_time						= 0;

static uint8_t 	total_time 								= 0;
static uint32_t sos_time								= 0;
static uint8_t 	current_time[3];

// GPS Command
static char 		gprmc_command[50] 				    = "$PSRF100,0,4800,8,1,0*0C";  // GPRMC, GPGGA, GPGSA default
//static char			gps_no_data[50]						= "$PMTK314,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*28";  // No Receive Data
//static char 		gps_dgps[14] 						= "$PMTK301,1*2D";																			// DGPS
//static char			gps_interval[17]					= "$PMTK220,2000*1C";																		// Interval (2 Second)

// LoRa Command
static char 		lora_baudrate[11]					= "AT+BAUD2 3";																					// Baudrate 9600
static char			lora_wakeup[11]						= "AT+WKT 255";																					// Wake up time (255 Second)
static char			lora_view1[6]						= "AT&V1";																						// Application EUI, Device EUI, Application Key							
static char			lora_ota_on[10]						= "AT+OTA 1";																					// Over the air on
static char			lora_ep[10]							= "AT+EP 1";																					// Enhenced provision on
static char			lora_conf[10]						= "AT+CONF 1";																					// Confirmed message on
static char			lora_txdr[11]						= "AT+TXDR 12";																					// Tx data rate SF12
static char			lora_txpw[11]						= "AT+TXPW 14";																					// Tx power 14
static char			lora_txch0[11]						= "AT+TXCH0 9";																					// 1st Tx Frequency : 922.1MHz
static char			lora_txch1[12]						= "AT+TXCH1 10";																				// 2nd Tx Frequency : 922.3MHz
static char			lora_txch2[12]						= "AT+TXCH2 11";																				// 3rd TX Frequency : 922.5MHz
static char			lora_rxch[10]						= "AT+RXCH 8";																					// Rx Frequency : 921.9MHz
static char			lora_rxdr[11]						= "AT+RXDR 12";																					// Rx data rate : SF12
static char			lora_adr[9]							= "AT+ADR 1";																					// Adaptive data rate on
static char			lora_txrn[10]						= "AT+TXRN ";																					// Tx retranse number
static char 		lora_save[5]						= "AT&W";																						// Save
static char 		lora_set_app_key[45]			    = "AT+PAK ";																					// Set Application Key
static char 		lora_send[105] 						= "AT+SEND ";																					// Send + Data

static char 		lora_at_log[45] = {'A','T','+','L','O','G','=','1','\r'};
static char 		lora_at_fwi[45] = "AT+FWI?";
static char 		lora_at_rst[45] = "AT+RST?";
static char 		lora_at_conf[45] = "AT+CONF?";
static char 		lora_at_deui[45] = {'A','T','+','D','E','U','I','?',' ','\r'};
static char 		lora_at_aeui[45] = "AT+AEUI?";
static char 		lora_at_akey[45] = "AT+AKEY?";


// UART Recieve
static char 		receive_data[200];																													// UART Receive data
static uint8_t 	p_index = 0;																															// UART Receive data index

// Command Queue
char			command[80];
char 			command_packet[4][80];																																	
uint8_t 	command_packet_cnt							= 0;
uint8_t 	command_packet_front						= 0;
uint8_t 	command_packet_rear							= 0;

// Packet Info
uint8_t		packet[46] = {0};

uint8_t 	packet_header[8];																																				// Packet header
uint8_t 	packet_version 								= 1;																										// Packet version
uint8_t 	major_minor[8];																																					// Major, Minor
uint8_t		lora_eui[8];																																						// LoRa Device EUI
uint32_t	timestamp									= 0;																										// Timestamp
uint8_t 	error_range[4] 								= {0x41, 0x20, 0x00, 0x00};															// Error range
uint8_t 	battery_state;																																					// Battery state					
uint8_t 	firmware_version[3] 						= {0x01, 0x01, 0x03};																		// Firmware version
uint8_t		max_xyz_noti[3];																																				// Gyro sensor

// Pstorage info
uint8_t 	time_schedule[20];																																			// Time schedule
uint8_t 	gps_gathering_cycle[4];																																	// GPS gathering cycle
uint8_t 	network_mode[4];																																				// Network mode
uint8_t		advertising_interval[4];																																// Advertising interval
uint8_t 	gps_fix_try_time[4];																																		// GPS fix try time
uint8_t		heart_beat_cycle[8];																																		// Heart beat cycle
uint8_t		sos_clear_time[4];																																			// SOS clear time
uint8_t 	lora_app_key[16];																																				// LoRa App Key
uint8_t		gyro_ignore[4];																																					// Gyro ignore
uint8_t 	first_boot_flag[4];																																			// First boot flag
uint8_t		lora_retranse_num[4];																																		// Lora retranse number
uint8_t		real_join_flag[4];																																			// Real Join flag

// Noti
static uint8_t 	ble_success[2] 						    = {0x00, 0x01};																					// BLE Setting Success
static uint8_t 	ble_fail[2] 							= {0x00, 0x00};																					// BLE Setting Fail

static uint8_t 	noti[20];

// SPI	
static uint8_t 	accel_rx[7];

static int16_t 	x_val;
static int16_t 	y_val;
static int16_t 	z_val;

static int8_t 	xyz_noti[3];
static int8_t	new_xyz_noti[3];
static int8_t	dif_xyz_noti[3];

// Gyro stop
static uint32_t gyro_stop_time;

// Test Power 
static uint8_t test_power_cnt  						= 0;
static uint8_t test_power_flag 						= 0;
static uint8_t test_gyro_check 						= 0;

static uint8_t app_key_check_flag 				    = 0;

// Battery Cut Off
static uint8_t battery_cut_off_cnt 				    = 0;

// Variable
static uint8_t app_beacon_uuid[16] 				    = {0x2e, 0x77, 0x13, 0xc8, 0x20, 0x9e, 0x11, 0xe6, 0xbb, 0xa7, 0x40, 0xf2, 0xe9, 0xcc, 0x23, 0xa2};
uint8_t 	set_app_key[16];

///////////////////////////////////////////////////  FUNCTION  /////////////////////////////////////////////////////////////////////////

void power_manage(void);
void advertising_init(void);
void advertising_start(void);
void uart_stop(void);
static void nus_data_handler(ble_nus_t * p_nus, uint8_t * p_data, uint16_t length);
static void uart_start(void);
static void battery_level_update(void);

// Fix gps timer start
void fix_gps_timer_start(void){
	gps_flag = 1;
	gps_uart_flag = 1;
	if(0 == nrf_gpio_pin_read(GPIO_GPS_PWR_PIN)){
		nrf_gpio_pin_set(GPIO_GPS_PWR_PIN);
	}
	app_timer_start(fix_gps_timer_id,FIX_GPS_TIMER_INTERVAL,NULL);
}

// Mode op timer stop
void mode_op_timer_stop(void){
	app_timer_stop(mode_op_timer_id);
}

// Factory Setting Success
void factory_setting_success(void){
	factory_setting_flag = 0;
		
	clear_led();
	power_off_flag = 1;
	led_state = 2;
}

// Setting Timer Start
void setting_timer_start(void){
	app_timer_start(lora_set_timer_id,LORA_SET_TIMER_INTERVAL,NULL);
}

// Get bit
uint8_t byte_from_bit(uint8_t get_byte, uint8_t get_bit){
	return (get_byte & (1 << get_bit)) >> get_bit;
}

/**@brief Function for stopping advertising.
 */
static void advertising_stop(void)
{
    uint32_t err_code;

    err_code = sd_ble_gap_adv_stop();
    APP_ERROR_CHECK(err_code);

}

/**@brief Function for loading application-specific context after establishing a secure connection.
 *
 * @details This function will load the application context and check if the ATT table is marked as
 *          changed. If the ATT table is marked as changed, a Service Changed Indication
 *          is sent to the peer if the Service Changed CCCD is set to indicate.
 *
 * @param[in] p_handle The Device Manager handle that identifies the connection for which the context
 *                     should be loaded.
 */
static void app_context_load(dm_handle_t const * p_handle)
{
    uint32_t                 err_code;
    static uint32_t          context_data;
    dm_application_context_t context;

    context.len    = sizeof(context_data);
    context.p_data = (uint8_t *)&context_data;
	
    err_code = dm_application_context_get(p_handle, &context);
    if (err_code == NRF_SUCCESS)
    {
        // Send Service Changed Indication if ATT table has changed.
        if ((context_data & (DFU_APP_ATT_TABLE_CHANGED << DFU_APP_ATT_TABLE_POS)) != 0)
        {
            err_code = sd_ble_gatts_service_changed(m_conn_handle, APP_SERVICE_HANDLE_START, BLE_HANDLE_MAX);
            if ((err_code != NRF_SUCCESS) &&
                (err_code != BLE_ERROR_INVALID_CONN_HANDLE) &&
                (err_code != NRF_ERROR_INVALID_STATE) &&
                (err_code != BLE_ERROR_NO_TX_PACKETS) &&
                (err_code != NRF_ERROR_BUSY) &&
                (err_code != BLE_ERROR_GATTS_SYS_ATTR_MISSING))
            {
                APP_ERROR_HANDLER(err_code);
            }
        }

        err_code = dm_application_context_delete(p_handle);
        APP_ERROR_CHECK(err_code);
				
    }
    else if (err_code == DM_NO_APP_CONTEXT)
    {
        // No context available. Ignore.
    }
    else
    {
        APP_ERROR_HANDLER(err_code);
    }
}


/** @snippet [DFU BLE Reset prepare] */
/**@brief Function for preparing for system reset.
 *
 * @details This function implements @ref dfu_app_reset_prepare_t. It will be called by
 *          @ref dfu_app_handler.c before entering the bootloader/DFU.
 *          This allows the current running application to shut down gracefully.
 */
static void reset_prepare(void)
{
    uint32_t err_code;
	
    if (m_conn_handle != BLE_CONN_HANDLE_INVALID)
    {
        // Disconnect from peer.
        err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
        APP_ERROR_CHECK(err_code);
    }
    else
    {
        // If not connected, the device will be advertising. Hence stop the advertising.
        advertising_stop();
    }

    err_code = ble_conn_params_stop();
    APP_ERROR_CHECK(err_code);

    nrf_delay_ms(500);
}
/** @snippet [DFU BLE Reset prepare] */


/**@brief Function for assert macro callback.
 *
 * @details This function will be called in case of an assert in the SoftDevice.
 *
 * @warning This handler is an example only and does not fit a final product. You need to analyse 
 *          how your product is supposed to react in case of Assert.
 * @warning On assert from the SoftDevice, the system can only recover on reset.
 *
 * @param[in] line_num    Line number of the failing ASSERT call.
 * @param[in] p_file_name File name of the failing ASSERT call.
 */
void assert_nrf_callback(uint16_t line_num, const uint8_t * p_file_name)
{
    app_error_handler(DEAD_BEEF, line_num, p_file_name);
}

static void power_timeout_handler(void *p_context){
	
	// LED 동작
	led_operation();
	
	if(0 == power_flag){
		
		if(0 == test_power_flag){
			if(nrf_gpio_pin_read(GPIO_POWER_BUTTON_PIN) == 1 && nrf_gpio_pin_read(GPIO_KEY_PIN) == 1){
				test_power_cnt++;
				debug_print(0,"Test Power Cnt : %d\n",test_power_cnt);
				
				battery_state = batt_adc_check();
				//debug_print(0,"Battery : %d\n",battery_state);
				
				if(45 == test_power_cnt){
					
					if(0 < battery_state){
						debug_print(0,"Test Power On\n");
						
						memset(&m_beacon_info[2],0,8);
						
						nrf_gpio_pin_set(GPIO_POWER_ON_PIN);
						nrf_gpio_pin_set(GPIO_GPS_V_PWR_PIN);
						nrf_gpio_pin_set(GPIO_GPS_PWR_PIN);
						nrf_gpio_pin_set(GPIO_LORA_PWR_PIN);
						
						app_timer_start(test_accel_timer_id,TEST_ACCEL_TIMER_INTERVAL,NULL);
						app_timer_start(test_init_timer_id,TEST_INIT_TIMER_INTERVAL,NULL);
						
						test_power_flag = 1;
						
						clear_led();
						led_state = 12;
						
						advertising_init();
						advertising_start();
					}
					else{
						debug_print(0,"Not Enough Battery\n");
					}
				}
			}	
			else{
				test_power_cnt = 0;
				
				if(nrf_gpio_pin_read(GPIO_POWER_BUTTON_PIN) == 1){
					power_cnt++;
					debug_print(0,"Power_cnt : %d\n",power_cnt);
					
					battery_state = batt_adc_check();
					//debug_print(0,"Battery : %d\n",battery_state);
					
					if(15 == power_cnt){
						
						if(0 < battery_state){
							debug_print(0,"Power on\n");
							
							nrf_gpio_pin_set(GPIO_POWER_ON_PIN);
							nrf_gpio_pin_set(GPIO_GPS_V_PWR_PIN);
							nrf_gpio_pin_set(GPIO_GPS_PWR_PIN);
							nrf_gpio_pin_set(GPIO_LORA_PWR_PIN);
                            
                            nrf_gpio_pin_set(GPIO_GPS_RESET);
						
							app_timer_start(setting_init_timer_id,SETTING_INIT_TIMER_INTERVAL,NULL);
							app_timer_start(rtc_timer_id,RTC_TIMER_INTERVAL,NULL);
							//app_timer_start(accel_timer_id,ACCEL_TIMER_INTERVAL,NULL);
							
							power_flag = 1;
							
							clear_led();
							led_state = 1;
							
							if(1 == network_mode[1]){
								advertising_flag = 1;
								advertising_init();
								debug_print(0,"Beacon Start\n");
								
								advertising_start();
							}
						}
						else{
							debug_print(0,"Not Enough Battery\n");
						}
					}
				}
				else{
					power_cnt = 0;
				}
			}
		}
		else{
			if(1 == nrf_gpio_pin_read(GPIO_POWER_BUTTON_PIN)){
				test_power_cnt++;
				debug_print(0,"Test Power Cnt : %d\n",test_power_cnt);
				if(15 == test_power_cnt){
					debug_print(0,"Test Power Off\n");
					
					nrf_gpio_pin_clear(GPIO_POWER_ON_PIN);
					nrf_gpio_pin_clear(GPIO_GPS_V_PWR_PIN);
					nrf_gpio_pin_clear(GPIO_GPS_PWR_PIN);
					nrf_gpio_pin_clear(GPIO_LORA_PWR_PIN);
					
					clear_led();
					led_state = 0;
					
					test_power_flag = 0;
					
					NVIC_SystemReset();
				}
			}
			else{
				test_power_cnt = 0;
			}
		}
	}
	
	else{

		if(nrf_gpio_pin_read(GPIO_POWER_BUTTON_PIN) == 1 && nrf_gpio_pin_read(GPIO_KEY_PIN) == 1){
			if(0 == factory_reset_flag){
				beacon_cnt ++;
				debug_print(0,"Beacon_cnt : %d\n",beacon_cnt);
				
				if(15 == beacon_cnt){
					
					// 로라 모드
					if(0 == network_mode[1]){
						
						if(0 == advertising_flag){
							advertising_flag = 1;
							
							memcpy(&m_beacon_info[2],app_beacon_uuid,16);
							
							advertising_init();
							debug_print(0,"Beacon Start\n");
							
							clear_led();
							led_state = 3;
							
							advertising_start();
						}
						
					}
					
					// 비콘 모드
					else if(1 == network_mode[1]){
						
						if(0 == fixed_uuid_flag){
							
							debug_print(0,"Fixed UUID\n");
							
							fixed_uuid_flag = 1;
							
							memcpy(&m_beacon_info[2],app_beacon_uuid,16);
							
							advertising_init();
							
						}
						
					}
					
				}
			}
			else{
				factory_reset_cnt++;
				debug_print(0,"Factory Reset Cnt : %d\n",factory_reset_cnt);
				
				// 타임스케쥴, 모드, adv interval, packet header, gps fix try time, sos clear 초기화
				if(50 == factory_reset_cnt){
					
					debug_print(0,"Factory Reset\n");
					
					memset(time_schedule,255,sizeof(time_schedule));
					memset(gps_gathering_cycle,255,sizeof(gps_gathering_cycle));
					memset(network_mode,255,sizeof(network_mode));
					memset(advertising_interval,255,sizeof(advertising_interval));
					memset(packet_header,255,sizeof(packet_header));
					memset(gps_fix_try_time,255,sizeof(gps_fix_try_time));
					memset(heart_beat_cycle,255,sizeof(heart_beat_cycle));
					memset(sos_clear_time,255,sizeof(sos_clear_time));
					memset(gyro_ignore,255,sizeof(gyro_ignore));
					
					storage_update(DEFAULT_DATA_BLOCK,time_schedule,20,8);
					storage_update(DEFAULT_DATA_BLOCK,gps_gathering_cycle,4,28);
					storage_update(DEFAULT_DATA_BLOCK,network_mode,4,32);
					storage_update(DEFAULT_DATA_BLOCK,advertising_interval,4,36);
					storage_update(DEFAULT_DATA_BLOCK,packet_header,8,40);
					storage_update(DEFAULT_DATA_BLOCK,gps_fix_try_time,4,48);
					storage_update(DEFAULT_DATA_BLOCK,heart_beat_cycle,8,52);
					storage_update(DEFAULT_DATA_BLOCK,sos_clear_time,4,60);
					storage_update(DEFAULT_DATA_BLOCK,gyro_ignore,4,84);
					
					clear_led();
					power_off_flag = 1;
					led_state = 2;
					
				}
			}
		}
		
		else{
			beacon_cnt = 0;
			factory_reset_cnt = 0;
		
			if(nrf_gpio_pin_read(GPIO_POWER_BUTTON_PIN) == 1){
				power_cnt++;
				debug_print(0,"Power_cnt : %d\n",power_cnt);
				
				if(15 == power_cnt){

					debug_print(0,"Power off\n");
					
					clear_led();
					power_off_flag = 1;
					led_state = 2;
				}
				
			}
			else{
				power_cnt = 0;
			}
			if(nrf_gpio_pin_read(GPIO_KEY_PIN) == 1){
				function_key_cnt++;
				debug_print(0,"Fucntion_key_cnt : %d\n",function_key_cnt);
				
				// 2초 눌렀을 경우 SOS 기능 동작
				if(20 == function_key_cnt){
					debug_print(0,"Function key LoRa Enqueue\n");
						
					lora_no_send_flag = 0;
					if(0 == function_key_flag){
						function_key_flag = 1;
					}
					sos_flag = 1;
						
					clear_led();
					led_state = 5;
						
					m_beacon_info[21] &= ~(0x01 << 0);	
					m_beacon_info[21] |= (0x01 << 0);
					advertising_init();
					
				}
				
				// 10초 눌렀을 경우 Factory Reset Ready
				if(100 == function_key_cnt){
					debug_print(0,"Factory Reset Ready\n");
					
					factory_reset_flag = 1;
					
					clear_led();
					led_state = 11;
				}
				
			}
			else{
				// Factory Reset Flag 가 없을 경우 일반 키 반응
				if(0 == factory_reset_flag){
					
					// 0 ~ 2초 사이로 눌렀을 경우 Power On Check
					if(0 < function_key_cnt && 20 > function_key_cnt){
						debug_print(0,"Power On Check\n");
						
						clear_led();
						led_state = 6;
					}
				}
				// Factory Reset Flag 가 있을 경우 Factory Reset Stop
				else{
					debug_print(0,"Factory Reset Stop\n");
					factory_reset_flag = 0;
					
					clear_led();
					led_state = 0;
				}
				function_key_cnt = 0;
			}
		}	
	}
}
#if 1
static void setting_init_timeout_handler(void *p_context){
	setting_init_cnt++;
	if(1 == setting_init_cnt){
     }
    /*
    if(1 == setting_init_cnt){
        uart_switch = 0;
        if(0 == nrf_gpio_pin_read(GPIO_GPS_PWR_PIN)) {
            nrf_gpio_pin_set(GPIO_GPS_PWR_PIN);
        }
    }
        
    else if(2 == setting_init_cnt){
        //MUC OE
        if(0 == nrf_gpio_pin_read(GPIO_GPS_RESET)) {
            nrf_gpio_pin_set(GPIO_GPS_RESET);
        }
    }
        
    else if(3 == setting_init_cnt){
        if(0 == nrf_gpio_pin_read(GPIO_GPS_SYSTEM_ON)) {
            nrf_gpio_pin_set(GPIO_GPS_SYSTEM_ON);
        }
        
        if(0 == nrf_gpio_pin_read(GPIO_GPS_ON_OFF)) {
            nrf_gpio_pin_set(GPIO_GPS_ON_OFF);
        }
    }
    
	else if(30 == setting_init_cnt){
		uart_start();
		gps_setting_flag = 1;
	}
    
    else if(35 == setting_init_cnt){
        char temp[]="$PSRF103,3,0,0,1*27";
		nus_data_handler(&m_nus,(uint8_t *)temp,strlen(temp));
    }
    
    else if(36 == setting_init_cnt){
        char temp[]="$PSRF103,9,0,0,1*2D";
		nus_data_handler(&m_nus,(uint8_t *)temp,strlen(temp));
    }
    
    else if(39 == setting_init_cnt){
        //setting_init_cnt = 36;
    }
    
	else if(40 == setting_init_cnt){
		uart_stop();
		gps_setting_flag = 1;
		nrf_gpio_pin_clear(GPIO_GPS_PWR_PIN);
	}
    */
	else if(61 == setting_init_cnt){
		uart_switch = 1;
		lora_setting_flag = 1;
		nrf_gpio_pin_set(GPIO_LORA_PWR_PIN);
		nrf_gpio_pin_set(GPIO_LORA_INTP_PIN);        
	}
    
    else if(62 == setting_init_cnt){
		uart_start();
		//nus_data_handler(&m_nus,(uint8_t *)lora_baudrate,strlen(lora_baudrate));
	}
    
    else if(65 == setting_init_cnt){
        //nus_data_handler(&m_nus,(uint8_t *)lora_at_log,strlen(lora_at_log));
	}
    
    else if(75 == setting_init_cnt){
        //nus_data_handler(&m_nus,(uint8_t *)lora_at_fwi,strlen(lora_at_fwi));
	}
    
    
    else if(85 == setting_init_cnt){
        nus_data_handler(&m_nus,(uint8_t *)lora_at_deui,strlen(lora_at_deui));
	}
    
    else if(95 == setting_init_cnt){
        //nus_data_handler(&m_nus,(uint8_t *)lora_at_aeui,strlen(lora_at_aeui));
	}
        
    else if(105 == setting_init_cnt){
        //nus_data_handler(&m_nus,(uint8_t *)lora_at_akey,strlen(lora_at_akey));
	}
           
    else if(115 == setting_init_cnt){
        setting_init_cnt = 64;
	}
}
#else
static void setting_init_timeout_handler(void *p_context){
	setting_init_cnt++;
    
    if(20 == setting_init_cnt){
        
        if(nrf_gpio_pin_read(GPIO_GPS_PWR_PIN) == 0)
        {
			nrf_gpio_pin_set(GPIO_GPS_PWR_PIN);
            nrf_delay_ms(500);
        }
        nrf_gpio_pin_set(GPIO_GPS_SYSTEM_ON);
        nrf_gpio_pin_set(GPIO_GPS_ON_OFF);
    }
    
    else if(21 == setting_init_cnt){
        uart_switch = 0;
        if(0 == nrf_gpio_pin_read(GPIO_GPS_RESET))
        {
            debug_print(0,"mjlee output enable!\n");
            nrf_gpio_pin_set(GPIO_GPS_RESET);
        }
    }
    
    else if(22 == setting_init_cnt){
        if(0 == nrf_gpio_pin_read(GPIO_GPS_SYSTEM_ON)){
            debug_print(0,"mjlee gps system on!\n");
            nrf_gpio_pin_set(GPIO_GPS_SYSTEM_ON);
        }
        if(0 == nrf_gpio_pin_read(GPIO_GPS_ON_OFF)){
            debug_print(0,"mjlee gps enable on!\n");
            nrf_gpio_pin_set(GPIO_GPS_ON_OFF);
        }
    }
    
	else if(26 == setting_init_cnt){
		uart_start();
	}
	
	else if(27 == setting_init_cnt){
		//nus_data_handler(&m_nus,(uint8_t *)gps_interval,strlen(gps_interval));
	}

	else if(28 == setting_init_cnt){
		//nus_data_handler(&m_nus,(uint8_t *)gps_no_data,strlen(gps_no_data));
	}
	
	else if(29 == setting_init_cnt){
		//nus_data_handler(&m_nus,(uint8_t *)gps_dgps,strlen(gps_dgps));
	}
	
	else if(30 == setting_init_cnt){
		uart_stop();
		gps_setting_flag = 1;
		nrf_gpio_pin_clear(GPIO_GPS_PWR_PIN);
	}
	else if(61 == setting_init_cnt){
		uart_switch = 1;
		lora_setting_flag = 1;
		nrf_gpio_pin_set(GPIO_LORA_INTP_PIN);
	}

	else if(62 == setting_init_cnt){
		uart_start();
		nus_data_handler(&m_nus,(uint8_t *)lora_baudrate,strlen(lora_baudrate));
	}	

	else if(63 == setting_init_cnt){
		uart_start();
		nus_data_handler(&m_nus,(uint8_t *)lora_wakeup,strlen(lora_wakeup));
	}
	
	else if(64 == setting_init_cnt){
		nus_data_handler(&m_nus,(uint8_t *)lora_view1,strlen(lora_view1));
	}
    #if 0
	else if(65 == setting_init_cnt){
        char temp[]="AT&F";
		nus_data_handler(&m_nus,(uint8_t *)temp,strlen(temp));
	}
    #endif
	else if(66 == setting_init_cnt){
		// 저장된 키가 있고 App key가 없을 때 app key 저장
        //mjlee, is_save_app_key == 1이면, 뭔가 있을 때, lora_app_key가 0이 아닐 때
        //debug_print(0,"setting_init_cnt, %d, %d\n",set_app_key[0], lora_app_key[0]);
		if(1 == is_save_app_key && 1 == app_key_check(lora_app_key)){
			char s_app_key[35];
			memset(s_app_key,0,sizeof(s_app_key));
			for(int i=0;i<16;i++)
				sprintf(&s_app_key[strlen(s_app_key)],"%02x",set_app_key[i]);
			strcat(lora_set_app_key,s_app_key);
			debug_print(0,"%s\n",lora_set_app_key);
			
			nus_data_handler(&m_nus,(uint8_t *)lora_set_app_key,strlen(lora_set_app_key));
		}
		else{
			debug_print(0,"Not Saved Appkey or Valid App Key\n");
			// App key가 없고 LoRa 모드일 때 자동으로 Advertsing
			if(1 == app_key_check(lora_app_key) && 0 == network_mode[1]){
                //mjlee
                //debug_print(0,"lora_app_key == 0\n");
				
				app_key_check_flag = 1;
				
				if(0 == advertising_flag){
					advertising_flag = 1;
						
					memcpy(&m_beacon_info[2],app_beacon_uuid,16);
						
					debug_print(0,"Beacon Start\n");
					
					clear_led();
					led_state = 3;
					
					advertising_init();					
					advertising_start();
				}
				
			}
		}
	}
	
	else if(67 == setting_init_cnt){
		nus_data_handler(&m_nus,(uint8_t *)lora_ota_on,strlen(lora_ota_on));
	}
	
	else if(68 == setting_init_cnt){
		nus_data_handler(&m_nus,(uint8_t *)lora_ep,strlen(lora_ep));
	}
	
	else if(69 == setting_init_cnt){
		nus_data_handler(&m_nus,(uint8_t *)lora_conf,strlen(lora_conf));
	}
	
	else if(70 == setting_init_cnt){
		nus_data_handler(&m_nus,(uint8_t *)lora_txdr,strlen(lora_txdr));
	}
	
	else if(71 == setting_init_cnt){
		nus_data_handler(&m_nus,(uint8_t *)lora_txpw,strlen(lora_txpw));
	}
	
	else if(72 == setting_init_cnt){
		nus_data_handler(&m_nus,(uint8_t *)lora_txch0,strlen(lora_txch0));
	}
	
	else if(73 == setting_init_cnt){
		nus_data_handler(&m_nus,(uint8_t *)lora_txch1,strlen(lora_txch1));
	}
	
	else if(74 == setting_init_cnt){
		nus_data_handler(&m_nus,(uint8_t *)lora_txch2,strlen(lora_txch2));
	}
	
	else if(75 == setting_init_cnt){
		nus_data_handler(&m_nus,(uint8_t *)lora_rxch,strlen(lora_rxch));
	}
	
	else if(76 == setting_init_cnt){
		nus_data_handler(&m_nus,(uint8_t *)lora_rxdr,strlen(lora_rxdr));
	}
	
	else if(77 == setting_init_cnt){
		nus_data_handler(&m_nus,(uint8_t *)lora_adr,strlen(lora_adr));
	}
	
	else if(78 == setting_init_cnt){
		#if 1
        char temp[2];
		sprintf(temp,"%d",lora_retranse_num[1]);
		strcat(lora_txrn,temp);
		nus_data_handler(&m_nus,(uint8_t *)lora_txrn,strlen(lora_txrn));
        #else
        char temp[] = "AT+TXRN 1";
		nus_data_handler(&m_nus,(uint8_t *)temp,strlen(temp));
        #endif
	}
	
	else if(79 == setting_init_cnt){
		nus_data_handler(&m_nus,(uint8_t *)lora_save,strlen(lora_save));
	}
	
	else if(80 == setting_init_cnt){
		setting_init_cnt = 0;
		nrf_gpio_pin_clear(GPIO_LORA_INTP_PIN);
		app_timer_stop(setting_init_timer_id);
		lora_setting_end_flag = 1;
		
		// 기기가 한번도 켜진적이 없으면 LoRa 설정을 적용시키기 위해 reset
		if(1 == first_boot_flag[1]){
            debug_print(0,"first boot ok!\n");
            memset(first_boot_flag,0,sizeof(first_boot_flag));
            storage_update(DEFAULT_DATA_BLOCK,first_boot_flag,4,88);
            //uart_stop();
            if(1 != app_key_check(lora_app_key) || app_key_check_flag == 0)
            {
                debug_print(0,"app key set ok!\n");
                uart_stop();
                device_reset_flag = 1;
            }
		}		
		else{
			// 비콘 모드일 때, LoRa 끄고 Mode op timer 시작
			if(1 == network_mode[1]){
				uart_stop();
				uart_switch = 0;
				nrf_gpio_pin_clear(GPIO_LORA_PWR_PIN);
				app_timer_start(mode_op_timer_id,MODE_OP_TIMER_INTERVAL,NULL);
			}
			// 로라 모드일 때, App Key Check해서 0이아니면 Mode op timer 시작
			else{
				if(0 == app_key_check_flag){
					app_timer_start(mode_op_timer_id,MODE_OP_TIMER_INTERVAL,NULL);
				}
			}
		}
	}
}
#endif
static void rtc_timeout_handler(void *p_context){
	
	// 초마다 배터리 상태 업데이트
	//battery_level_update();
	
	time.second++;
	timestamp++;
	
	//debug_print(0,"Time Stamp : %d\n",timestamp);
	
	if( 60 == time.second ){
		time.second = 0;
		time.minute++;
	}
	if( 60 == time.minute ){
		time.minute = 0;
		time.hour++;
	}
	if( 24 == time.hour ){
		time.hour = 0;
		time.day++;
	}
	if( 7 == time.day ){
		time.day = 0;
	}
	debug_print(0,"Day : %d, Time : %02d : %02d : %02d\n",time.day,time.hour,time.minute,time.second);
	
	total_time = ((time.hour * 3600) + (time.minute * 60) + time.second)/ 600;
	
	if(1 == sos_flag){
		sos_time++;
		debug_print(0,"SOS Time : %d, %d\n",sos_time, sos_clear_time[1]);
		if(sos_clear_time[1]*60 == sos_time){
			sos_flag = 0;
			sos_time = 0;
			m_beacon_info[21] &= ~0x01;
		}
	}
	
	// Beacon 모드, UUID 고정이 아닐 때 UUID에 x,y,z 담기
	if(1 == network_mode[1] && 0 == fixed_uuid_flag){	
		memcpy(&m_beacon_info[2],xyz_noti,sizeof(xyz_noti));
		advertising_init();
		
	}
	
	if(1 == device_reset_flag){
		device_reset_cnt++;
		if(device_reset_cnt == 2){
			device_reset_cnt = 0;
			device_reset();
		}
	}
	
	// GPS가 한번이라도 잡혔을 때
	if(1 == gps_valid_flag){
		// GPS 수집 주기가 1분 이하이고, Gyro Ignore가 OFF일 때
		if(6 >= gps_gathering_cycle[1] && 0 == gyro_ignore[1]){
			// 자이로가 멈췄을 때
			if(0 == gyro_flag){
				gyro_stop_time++;
				debug_print(0,"Gyro Stop Time : %d\n",gyro_stop_time);
			}
			// 자이로가 움직일 때
			else{
				gyro_stop_time = 0;
				debug_print(0,"Gyrp Stop\n");
			}
			
			// 자이로가 멈춘지 30분이 지나면 gps 전원 OFF
			if(1800 <= gyro_stop_time){
				nrf_gpio_pin_clear(GPIO_GPS_PWR_PIN);
				debug_print(0,"Gyro Stop, GPS Off\n");
			}
		}
	}
}

static void accel_timeout_handler(void *p_context){	
	uint8_t no_previous_data=0;
	bma250e_spi_readAccelerometerData(accel_rx,sizeof(accel_rx),&x_val,&y_val,&z_val,new_xyz_noti);  
	
	//debug_print(0,"New Value : %d %d %d\n",new_xyz_noti[0],new_xyz_noti[1],new_xyz_noti[2]);
	//debug_print(0,"Pre Value : %d %d %d\n",xyz_noti[0],xyz_noti[1],xyz_noti[2]);
	
	if(0 == xyz_noti[0] && 0 == xyz_noti[1] && 0 == xyz_noti[2]){
		no_previous_data = 1;
	}
		
	if(0 == no_previous_data){
		dif_xyz_noti[0] = new_xyz_noti[0] - xyz_noti[0];
		dif_xyz_noti[1] = new_xyz_noti[1] - xyz_noti[1];
		dif_xyz_noti[2] = new_xyz_noti[2] - xyz_noti[2];
		
		//debug_print(0,"Dif Value : %d %d %d\n",abs(dif_xyz_noti[0]),abs(dif_xyz_noti[1]),abs(dif_xyz_noti[2]));
		
		if(10 <= abs(dif_xyz_noti[0]) || 10 <= abs(dif_xyz_noti[1]) || 10 <= abs(dif_xyz_noti[2])){
			//debug_print(0,"Value : %d %d %d\n",abs(dif_xyz_noti[0]),abs(dif_xyz_noti[1]),abs(dif_xyz_noti[2]));
			debug_print(0,"Gyro Moving\n");
			gyro_flag = 1;
		}
		
		if(max_xyz_noti[0] <= dif_xyz_noti[0]){
			max_xyz_noti[0] = abs(dif_xyz_noti[0]);
		}
		if(max_xyz_noti[1] <= dif_xyz_noti[1]){
			max_xyz_noti[1] = abs(dif_xyz_noti[1]);
		}
		if(max_xyz_noti[2] <= dif_xyz_noti[2]){
			max_xyz_noti[2] = abs(dif_xyz_noti[2]);
		}
		//debug_print(0,"Max Value : %d %d %d\n",max_xyz_noti[0],max_xyz_noti[1],max_xyz_noti[2]);
	}
	else{
		memcpy(max_xyz_noti,new_xyz_noti,sizeof(new_xyz_noti));
		//debug_print(0,"Max Value : %d %d %d\n",max_xyz_noti[0],max_xyz_noti[1],max_xyz_noti[2]);
	}
	
	memcpy(xyz_noti,new_xyz_noti,sizeof(xyz_noti));
}

static void mode_op_timeout_handler(void *p_context){
    //로라 모드일 때
    if(0 == network_mode[1]){
		// 개통이 안 되었을 때 Red LED
		if(0 == provision_success_flag){
			if(17 != led_state && (0 == led_state || 9 == led_state || 10 == led_state)){
				clear_led();
				led_state = 17;
			}
		}
		// 개통이 되면 uart stop
		else if(1 == provision_success_flag){
			provision_success_flag = 2;
			uart_stop();
		}
		
		heart_beat_time++;
		
		debug_print(0,"Heart Beat Time : %d\n",heart_beat_time);
		
		// Heart Beat 시간마다 Enqueue
		if(heart_beat_time == heart_beat_cycle_time){
			debug_print(0,"Heart Beat Enqueue\n");
			lora_no_send_flag = 0;
			heart_beat_time = 0;
			heart_beat_flag = 1;
		}
		
		// provision_try_flag 와 provision_flag 가 0이면 5번 개통 시도
		if(0 == provision_try_flag && 0 == provision_flag){
			debug_print(0,"Provision Try\n");
			// real_join_flag 0일 때, 8번 개통 시도
			if(0 == real_join_flag[1] && 8 == provision_fail_cnt){					
				debug_print(0,"Provision Fail\n");
				provision_fail_cnt = 0;
				uart_stop();
                
				if(1 == nrf_gpio_pin_read(GPIO_LORA_PWR_PIN)){
					nrf_gpio_pin_clear(GPIO_LORA_PWR_PIN);
				}
				provision_try_flag = 1;
					
				gps_gathering_time = 0;
					
				if(0 == gps_valid_flag){
					rtc_try_flag = 0;
				}
				else{
					fix_gps_timer_start();
				}	
			}
		}
		
		// 3분간 RTC를 잡기 위해 GPS ON
		else if(0 == rtc_try_flag && 0 == gps_valid_flag){
			debug_print(0,"GPS Try\n");
			gps_gathering_time++;
			
			if(0 == nrf_gpio_pin_read(GPIO_GPS_PWR_PIN)){
				debug_print(0,"GPS Power On\n");
				nrf_gpio_pin_set(GPIO_GPS_PWR_PIN);
			}
			
			//3분간 GPS PWR ON하면서 10초에 한번 UART Start, gps fix try time이 되면 GPS PWR OFF
			gps_fix_second++;
			debug_print(0,"GPS Fix Second : %d\n",gps_fix_second);
			
			//3분간 RTC 설정이 실패 했을 때
			if(gps_fix_try_time[1]*60 == gps_fix_second){
				debug_print(0,"GPS Fix Timeout\n");
				gps_fix_second = 0;
				gps_gathering_time = 0;
				gyro_flag = 0;
				if(1 == nrf_gpio_pin_read(GPIO_GPS_PWR_PIN)){
					debug_print(0,"GPS Power Off\n");
					nrf_gpio_pin_clear(GPIO_GPS_PWR_PIN);
				}
				rtc_try_flag = 1;
			}
			
			if(10 == gps_gathering_time){
				gps_gathering_time = 0;
				gps_flag = 1;
				gps_uart_flag = 1;
				app_timer_start(no_fix_gps_timer_id,NO_FIX_GPS_TIMER_INTERVAL,NULL);
			}
		}
	
		else{			
			gps_gathering_time++;
			
			debug_print(0,"GPS Gathering Time : %d\n",gps_gathering_time);

			// 로라 개통이 완료되고, lora_no_send_flag가 없고, LoRa UART나 GPS UART를 사용하지 않을 때 Function Key, Heart Beat 체크
			if((0 == lora_uart_flag && 0 == gps_uart_flag && 0 == lora_no_send_flag && 1 == provision_flag) && (0 != function_key_flag || 1 == heart_beat_flag)){
				lora_uart_flag = 1;
				packet_initialize();
				app_timer_start(lora_timer_id,LORA_TIMER_INTERVAL,NULL);
			}
			
			// RTC가 설정이 안 됐을 때
			if(0 == gps_valid_flag){				
				debug_print(0,"Not RTC Setting\n");
				// GPS 수집 주기가 되었을 때
				if(gps_gathering_time == gps_gathering_cycle[1]*10){
					// UART를 사용하지 않을 때
					if(0 == gps_uart_flag && 0 == lora_uart_flag){						
						// 자이로 무시 안할 때
						if(0 == gyro_ignore[1]){
							debug_print(0,"Gyro Not Ignore\n");
							if(1 == gyro_flag){
								debug_print(0,"Gyro Moving\n");								
								// 개통이 안 됐을 때
								if(0 == provision_flag){
									debug_print(0,"Not Provision\n");
									if(0 == nrf_gpio_pin_read(GPIO_LORA_PWR_PIN)){
										nrf_gpio_pin_set(GPIO_LORA_PWR_PIN);
										app_timer_start(lora_on_timer_id,LORA_ON_TIMER_INTERVAL,NULL);
									}
								}
								// RTC가 설정이 안 됐을 때
								else if(0 == gps_valid_flag){
									rtc_try_flag = 0;
								}		
							}
						}
						// 자이로 무시할 때
						else{
							debug_print(0,"Gyro Ignore\n");							
							// 개통이 안 됐을 때
							if(0 == provision_flag){
								debug_print(0,"Not Provision\n");
								if(0 == nrf_gpio_pin_read(GPIO_LORA_PWR_PIN)){
									nrf_gpio_pin_set(GPIO_LORA_PWR_PIN);
									app_timer_start(lora_on_timer_id,LORA_ON_TIMER_INTERVAL,NULL);
								}
							}
							// RTC가 설정이 안 됐을 때
							else if(0 == gps_valid_flag){
								rtc_try_flag = 0;
							}
						}
						gps_gathering_time = 0;
					}
					// UART 사용 중일 때
					else{
						debug_print(0,"GPS UART Using or LoRa UART Using\n");
						gps_gathering_time--;
					}
				}
			}
			// RTC 설정이 됐을 때
			else{
				// Time Schedule 체크
				//if( 1 == byte_from_bit(time_schedule[(total_time/8)+1],(7-(total_time%8))) && 1 == byte_from_bit(time_schedule[19],(7-time.day)) ){
                if(1){
					debug_print(0,"Time Schedule In\n");							
					if( gps_gathering_time == gps_gathering_cycle[1]*10){								
						if(0 == gps_uart_flag && 0 == lora_uart_flag){
							// 자이로를 무시하지 않을 때
							if( 0 == gyro_ignore[1] ){
								debug_print(0,"Gyro Not Ignore\n");
								// 자이로 체크
								if( 1 == gyro_flag){
									// LoRa 개통이 안 됐을 때
									if(0 == provision_flag){										
										if(0 == nrf_gpio_pin_read(GPIO_LORA_PWR_PIN)){
											nrf_gpio_pin_set(GPIO_LORA_PWR_PIN);
											app_timer_start(lora_on_timer_id,LORA_ON_TIMER_INTERVAL,NULL);
										}
									}
									else{
										fix_gps_timer_start();
									}
								}
								// 자이로 체크 안됬을때
								else{
									debug_print(0,"No Moving\n");
								}
							}
							// 자이로를 무시할 때
							else{
								debug_print(0,"Gyro Ignore\n");
								if(0 == provision_flag){										
									if(0 == nrf_gpio_pin_read(GPIO_LORA_PWR_PIN)){
										nrf_gpio_pin_set(GPIO_LORA_PWR_PIN);
										app_timer_start(lora_on_timer_id,LORA_ON_TIMER_INTERVAL,NULL);
									}
								}
								else{
									fix_gps_timer_start();	
								}
							}
							gps_gathering_time = 0;	
						}
						else{
							debug_print(0,"GPS UART Using or LoRa UART Using\n");
							gps_gathering_time--;
						}
					}
				}	
				// 타임스케쥴이 아닐 때
				else{
					debug_print(0,"Time Schedule Out\n");
					gps_gathering_time = 0;
					if(1 == nrf_gpio_pin_read(GPIO_GPS_PWR_PIN) && 0 == gps_uart_flag){
						debug_print(0,"GPS Power Off\n");
						nrf_gpio_pin_clear(GPIO_GPS_PWR_PIN);
					}
				}
			}
		}
	}
	
	//비콘 모드일 때
	else if(1 == network_mode[1]){		
		gps_gathering_time++;
		debug_print(0,"GPS Gathering Time : %d\n",gps_gathering_time);
		
		//처음 GPS를 Fix할 때
		if(0 == gps_valid_flag){
			//3분간 GPS PWR ON하면서 10초에 한번 UART Start, gps fix try time이 되면 GPS PWR OFF
			if(0 == gps_fix_out_flag){
				gps_fix_second++;
				debug_print(0,"GPS Fix Second : %d\n",gps_fix_second);
				if(0 == nrf_gpio_pin_read(GPIO_GPS_PWR_PIN)){
					debug_print(0,"GPS Power On\n");
					nrf_gpio_pin_set(GPIO_GPS_PWR_PIN);
				}
				if(gps_fix_try_time[1]*60 == gps_fix_second){
					debug_print(0,"GPS Fix Timeout\n");
					gps_fix_out_flag = 1;
					gps_fix_second = 0;
					gps_gathering_time = 0;
				}
				else if(10 == gps_gathering_time){
					gps_gathering_time = 0;
					gps_flag = 1;
					app_timer_start(no_fix_gps_timer_id,NO_FIX_GPS_TIMER_INTERVAL,NULL);
				}
			}
			//GPS PWR OFF, 위치수집주기가 되면 GPS PWR ON
			else{
				if(1 == nrf_gpio_pin_read(GPIO_GPS_PWR_PIN) && 0 == gps_uart_flag){
					debug_print(0,"GPS Power Off\n");
					nrf_gpio_pin_clear(GPIO_GPS_PWR_PIN);
				}
				if(gps_gathering_time == (gps_gathering_cycle[1]*10)){
					debug_print(0,"GPS Fix Time\n");
					gps_fix_out_flag = 0;
					gps_gathering_time = 0;
				}
			}
		}
		// GPS가 Fix 되고 타임스케쥴, 위치수집주기를 체크할 때
		else{
			// 타임스케쥴 체크 
			if( 1 == byte_from_bit(time_schedule[(total_time/8)+1],(7-(total_time%8))) && 1 == byte_from_bit(time_schedule[19],(7-time.day)) ) {
				debug_print(0,"Time Schedule In\n");
				// GPS UART를 사용하지 않을 때
				if( 0 == gps_uart_flag ){
					
					if( gps_gathering_time == gps_gathering_cycle[1]*10){
						
						if( 0 == gyro_ignore[1] ){
							debug_print(0,"Gyro Not Ignore\n");
							// 자이로 체크
							if( 1 == gyro_flag){
								fix_gps_timer_start();
							}
							else{
								debug_print(0,"No Moving\n");
							}
							gps_gathering_time = 0;
						}
						else{
							debug_print(0,"Gyro Ignore\n");
							fix_gps_timer_start();
						}
					}
				}
				// GPS UART를 사용 중일 때
				else{
					debug_print(0,"GPS UART Using\n");
					if( gps_gathering_time >= gps_gathering_cycle[1]*10){
						gps_gathering_time = gps_gathering_cycle[1]*10 - 1;
					}
				}
			}
			// 타임스케쥴이 아닐 때
			else{
				debug_print(0,"Time Schedule Out\n");
				gps_gathering_time = 0;
				if(1 == nrf_gpio_pin_read(GPIO_GPS_PWR_PIN) && 0 == gps_uart_flag){
					debug_print(0,"GPS Power Off\n");
					nrf_gpio_pin_clear(GPIO_GPS_PWR_PIN);
				}
			}		
		}	
	}
}

static void no_fix_gps_timeout_handler( void *p_context ) {	
	if(0 == no_fix_gps_uart_stop_flag){
		no_fix_gps_cnt++;
		if(1 == no_fix_gps_cnt){
			uart_switch = 0;
			uart_start();
		}
		else if(2 == no_fix_gps_cnt){
			nus_data_handler(&m_nus,(uint8_t *)gprmc_command,strlen(gprmc_command));
			gps_wait_flag = 1;
		}
	}
	else{
		// 3D Fix가 안 잡혔을 때
		if(0 == gps_valid_flag){
			if(1 == gps_wait_flag){
				gps_wait_flag = 0;
				no_fix_gps_cnt = 0;
				gps_no_data_flag = 1;
				//nus_data_handler(&m_nus,(uint8_t *)gps_no_data,strlen(gps_no_data));
			}
			
			no_fix_gps_cnt++;
			
			if(0 == gps_no_data_flag || 5 == no_fix_gps_cnt){
				gps_no_data_flag = 0;
				no_fix_gps_cnt = 0;
				no_fix_gps_uart_stop_flag = 0;
				
				// Packet의 GPS 값 초기화
				memset(&packet[18],0,12);
				memset(&packet[33],0,1);
				packet[39] &= ~(0x03 << 3);
				
				uart_stop();
				app_timer_stop(no_fix_gps_timer_id);
			}
		}
		// 3D Fix가 되었을 때 위성 6개 이상 잡기 시도
		else{
			// 6개 이상 안 잡히면 30초까지 시도 후 LoRa 전송
			if(0 == satellite_flag){
				satellite_wait_time++;
				if(300 <= satellite_wait_time){
					if(1 == gps_wait_flag){
						gps_wait_flag = 0;
						no_fix_gps_cnt = 0;
						gps_no_data_flag = 1;
						//nus_data_handler(&m_nus,(uint8_t *)gps_no_data,strlen(gps_no_data));
					}
					
					no_fix_gps_cnt++;
					
					if(0 == gps_no_data_flag || 5 == no_fix_gps_cnt){
						gps_no_data_flag = 0;
						no_fix_gps_cnt = 0;
						no_fix_gps_uart_stop_flag = 0;
						satellite_wait_time = 0;
						satellite_flag = 0;
						uart_stop();
						if(6 < gps_gathering_cycle[1]){
							nrf_gpio_pin_clear(GPIO_GPS_PWR_PIN);
						}
						app_timer_stop(no_fix_gps_timer_id);
						lora_no_send_flag = 0;
						packet_initialize();
						app_timer_start(lora_timer_id,LORA_TIMER_INTERVAL,NULL);
					}
				}
			}
			// 6개 이상 잡혔을 때
			else{
				if(1 == gps_wait_flag){
					gps_wait_flag = 0;
					no_fix_gps_cnt = 0;
					gps_no_data_flag = 1;
					//nus_data_handler(&m_nus,(uint8_t *)gps_no_data,strlen(gps_no_data));
				}
					
				no_fix_gps_cnt++;
					
				if(0 == gps_no_data_flag || 5 == no_fix_gps_cnt){
					gps_no_data_flag = 0;
					no_fix_gps_cnt = 0;
					no_fix_gps_uart_stop_flag = 0;
					satellite_wait_time = 0;
					satellite_flag = 0;
					uart_stop();
					if(6 < gps_gathering_cycle[1]){
						nrf_gpio_pin_clear(GPIO_GPS_PWR_PIN);
					}
					app_timer_stop(no_fix_gps_timer_id);
					lora_no_send_flag = 0;
					packet_initialize();
					lora_uart_flag = 1;
					app_timer_start(lora_timer_id,LORA_TIMER_INTERVAL,NULL);
				}
			}
		}
	}
}
	
static void fix_gps_timeout_handler(void *p_context){
	
	if(0 == fix_gps_uart_stop_flag){
		fix_gps_cnt++;
		// 전원 넣고 0.5초 뒤에 커맨드 날려서 데이터 계속 받도록
		if(2 == fix_gps_cnt){
			uart_switch = 0;
			uart_start();
		}
		else if(5 == fix_gps_cnt){
			nus_data_handler(&m_nus,(uint8_t *)gprmc_command,strlen(gprmc_command));
			gps_wait_flag = 1;
		}
		// 위치 수집 주기가 1분 이하일 때, GPS를 키고 30초 동안 Fix 못하면 UART stop
		if(6 >= gps_gathering_cycle[1]){
			if(300 <= fix_gps_cnt){
				if(1 == gps_wait_flag){
					gps_wait_flag = 0;
					gps_no_data_flag = 1;
					//nus_data_handler(&m_nus,(uint8_t *)gps_no_data,strlen(gps_no_data));
				}
				if(0 == gps_no_data_flag || 305 <= fix_gps_cnt){
					gps_no_data_flag = 0;
					fix_gps_cnt = 0;
					gyro_flag = 0;
					
					clear_led();
					led_state = 7;
					
					// Packet의 GPS 값 초기화
					memset(&packet[18],0,12);
					memset(&packet[33],0,1);
					packet[39] &= ~(0x03 << 3);
					
					uart_stop();
					app_timer_stop(fix_gps_timer_id);
				}
			}
		}
		// 위치 수집 주기가 1분을 초과할 때, GPS를 키고 3분 동안 Fix 못하면 UART stop 후 GPS 전원 OFF
		else{
			if(1800 <= fix_gps_cnt){
				if(1 == gps_wait_flag){
					gps_wait_flag = 0;
					gps_no_data_flag = 1;
					//nus_data_handler(&m_nus,(uint8_t *)gps_no_data,strlen(gps_no_data));
				}
				if(0 == gps_no_data_flag || 1805 <= fix_gps_cnt){
					gps_no_data_flag = 0;
					fix_gps_cnt = 0;
					gyro_flag = 0;
					
					clear_led();
					led_state = 7;
					
					// Packet의 GPS 값 초기화
					memset(&packet[18],0,12);
					memset(&packet[33],0,1);
					packet[39] &= ~(0x03 << 3);
					
					uart_stop();
					nrf_gpio_pin_clear(GPIO_GPS_PWR_PIN);
					app_timer_stop(fix_gps_timer_id);
				}
			}
		}
	}
	else{
		// 3D Fix가 됐을 때 30초 동안 대기하면서 위성이 6개가 될 때 까지 기다림
		if(0 == satellite_flag){
			satellite_wait_time++;
			if(300 <= satellite_wait_time){
				if(1 == gps_wait_flag){
					gps_wait_flag = 0;
					fix_gps_cnt = 0;
					gps_no_data_flag = 1;
					//nus_data_handler(&m_nus,(uint8_t *)gps_no_data,strlen(gps_no_data));
				}
				
				fix_gps_cnt++;
				
				if(0 == gps_no_data_flag || 5 == fix_gps_cnt){
					gps_no_data_flag = 0;
					fix_gps_cnt = 0;
					fix_gps_uart_stop_flag = 0;
					satellite_flag = 0;
					satellite_wait_time = 0;
					uart_stop();
					if(6 < gps_gathering_cycle[1]){
						nrf_gpio_pin_clear(GPIO_GPS_PWR_PIN);
					}
					app_timer_stop(fix_gps_timer_id);
					lora_no_send_flag = 0;
					packet_initialize();
					lora_uart_flag = 1;
					app_timer_start(lora_timer_id,LORA_TIMER_INTERVAL,NULL);
				}
			}
		}
		else{
			if(1 == gps_wait_flag){
				gps_wait_flag = 0;
				fix_gps_cnt = 0;
				gps_no_data_flag = 1;
				//nus_data_handler(&m_nus,(uint8_t *)gps_no_data,strlen(gps_no_data));
			}
			
			fix_gps_cnt++;
			
			if(0 == gps_no_data_flag || 5 == fix_gps_cnt){
				gps_no_data_flag = 0;
				fix_gps_cnt = 0;
				fix_gps_uart_stop_flag = 0;
				satellite_flag = 0;
				satellite_wait_time = 0;
				uart_stop();
				if(6 < gps_gathering_cycle[1]){
					nrf_gpio_pin_clear(GPIO_GPS_PWR_PIN);
				}
				app_timer_stop(fix_gps_timer_id);
				lora_no_send_flag = 0;
				packet_initialize();
				lora_uart_flag = 1;
				app_timer_start(lora_timer_id,LORA_TIMER_INTERVAL,NULL);
			}
		}
	}
}

static void lora_timeout_handler(void *p_context){
	lora_cnt++;
	if(0 == lora_uart_stop_flag){
		if(1 == lora_cnt){
			uart_switch = 1;
			nrf_gpio_pin_set(GPIO_LORA_INTP_PIN);		
			char s_packet[95];
			memset(s_packet,0,sizeof(s_packet));
			for(int i=0;i<sizeof(packet);i++){
				sprintf(&s_packet[strlen(s_packet)],"%02x",packet[i]);
			}	
			strcat(lora_send,s_packet);
			// SOS Status를 실었다면 Flag를 2로 변경
			if(1 == function_key_flag){
				function_key_flag = 2;
			}
		}
		
		else if(5 == lora_cnt){
			uart_start();
		}
		
		else if(7 == lora_cnt){
			nus_data_handler(&m_nus,(uint8_t *)lora_send,strlen(lora_send));
			memset(&lora_send[8],0,strlen(lora_send)-8);
		}		
	}
	
	else{
		nrf_gpio_pin_clear(GPIO_LORA_INTP_PIN);
		lora_cnt = 0;
		lora_uart_stop_flag = 0;
		uart_stop();
		app_timer_stop(lora_timer_id);
	}
}

static void lora_set_timeout_handler(void *p_context){
	lora_set_cnt++;
	if(1 == lora_set_cnt){
		nrf_gpio_pin_set(GPIO_LORA_INTP_PIN);
	}
	else{
		if(0 == command_packet_cnt){
			lora_set_cnt = 0;
			nrf_gpio_pin_clear(GPIO_LORA_INTP_PIN);
			app_timer_stop(lora_set_timer_id);
		}
		else{
			nus_data_handler(&m_nus,(uint8_t *)&command_packet[command_packet_front][1],command_packet[command_packet_front][0]);
			command_dequeue();
		}
	}
}

static void lora_on_timeout_handler(void *p_context){
	lora_on_cnt++;
	
	if(1 == lora_on_cnt){
		nrf_gpio_pin_set(GPIO_LORA_PWR_PIN);
	}
	else if(61 == lora_on_cnt){
		nrf_gpio_pin_set(GPIO_LORA_INTP_PIN);
	}
	else if(62 == lora_on_cnt){
		lora_setting_end_flag = 0;
		lora_setting_flag = 1;
		uart_switch = 1;
		uart_start();
		nus_data_handler(&m_nus,(uint8_t *)lora_baudrate,strlen(lora_baudrate));
	}
	else if(63 == lora_on_cnt){
		char temp[2];
		uart_start();
		memset(&lora_txrn[8],0,sizeof(lora_txrn)-8);
		sprintf(temp,"%d",lora_retranse_num[1]);
		strcat(lora_txrn,temp);
		nus_data_handler(&m_nus,(uint8_t *)lora_txrn,strlen(lora_txrn));
	}
	else if(64 == lora_on_cnt){
		lora_on_cnt = 0;
		nrf_gpio_pin_clear(GPIO_LORA_INTP_PIN);
		app_timer_stop(lora_on_timer_id);
		lora_setting_end_flag = 1;
		provision_try_flag = 0;
	}
}

static void test_accel_timeout_handler(void *p_context){
	bma250e_spi_readAccelerometerData(accel_rx,sizeof(accel_rx),&x_val,&y_val,&z_val,xyz_noti);
	memcpy(&m_beacon_info[2],xyz_noti,sizeof(xyz_noti));
	advertising_init();
}

static void test_init_timeout_handler(void *p_context){
	test_init_cnt++;
	
	if(1 == test_init_cnt){
		uart_switch = 0;
		uart_start();
	}
	else if(20 == test_init_cnt){
		uart_stop();
	}
	else if(121 == test_init_cnt){
		uart_switch = 1;
		nrf_gpio_pin_set(GPIO_LORA_INTP_PIN);
	}
	else if(123 == test_init_cnt){
		test_lora_flag = 1;
		uart_start();
		nus_data_handler(&m_nus,(uint8_t *)"AT",strlen("AT"));
	}
	else if(125 == test_init_cnt){
		uart_stop();
		test_lora_flag = 0;
		lora_setting_flag = 1;
		uart_start();
		nus_data_handler(&m_nus,(uint8_t *)lora_baudrate,strlen(lora_baudrate));
	}
	else if(127== test_init_cnt){
		uart_start();
		nus_data_handler(&m_nus,(uint8_t *)"AT",strlen("AT"));
	}
	else if(130 == test_init_cnt){
		test_init_cnt = 0;
		uart_stop();
		nrf_gpio_pin_clear(GPIO_LORA_INTP_PIN);
		nrf_gpio_pin_clear(GPIO_LORA_PWR_PIN);
		uart_switch = 0;
		uart_start();
		app_timer_stop(test_init_timer_id);
		app_timer_start(test_fix_timer_id,TEST_FIX_TIMER_INTERVAL,NULL);
	}
}

static void test_fix_timeout_handler(void *p_context){
	
	if(0 == test_result_flag){
		test_gps_fix_cnt++;
		if(180 == test_gps_fix_cnt){
			test_gps_fix_cnt = 0;
			test_result_flag = 1;
		}
	}
	else{
		if((0 == m_beacon_info[2] && 0 == m_beacon_info[3] && 0 == m_beacon_info[4])
			|| (255 == m_beacon_info[2] && 255 == m_beacon_info[3] && 255 == m_beacon_info[4])){
				debug_print(0,"Gyro Test Fail\n");
				test_gyro_check = 1;
		}
			
		if(0 == m_beacon_info[5] || 0 == m_beacon_info[6] || 0 == m_beacon_info[7] || 0 == m_beacon_info[8] || 0 == m_beacon_info[9] || 1 == test_gyro_check){
			debug_print(0,"Test Fail\n");
			clear_led();
			led_state = 14;
		}
		else if(0 == lora_baudrate_test){
			debug_print(0,"Test LoRa Baudrate Fail\n");
			clear_led();
			led_state = 15;
		}
		else{
			debug_print(0,"Test Success\n");
			clear_led();
			led_state = 13;
		}
		
		test_gps_fix_cnt = 0;
		app_timer_stop(test_fix_timer_id);
	}
		
}


static void timers_init(void){
	uint32_t err_code;
	
	APP_TIMER_INIT(APP_TIMER_PRESCALER, APP_TIMER_OP_QUEUE_SIZE, false);
	
	err_code = app_timer_create(&power_timer_id,APP_TIMER_MODE_REPEATED,power_timeout_handler);
	APP_ERROR_CHECK(err_code);
	
	err_code = app_timer_create(&setting_init_timer_id,APP_TIMER_MODE_REPEATED,setting_init_timeout_handler);
	APP_ERROR_CHECK(err_code);

	err_code = app_timer_create(&rtc_timer_id,APP_TIMER_MODE_REPEATED,rtc_timeout_handler);
	APP_ERROR_CHECK(err_code);
	
	err_code = app_timer_create(&accel_timer_id,APP_TIMER_MODE_REPEATED,accel_timeout_handler);
	APP_ERROR_CHECK(err_code);
	
	err_code = app_timer_create(&mode_op_timer_id,APP_TIMER_MODE_REPEATED,mode_op_timeout_handler);
	APP_ERROR_CHECK(err_code);
	
	err_code = app_timer_create(&no_fix_gps_timer_id,APP_TIMER_MODE_REPEATED,no_fix_gps_timeout_handler);
	APP_ERROR_CHECK(err_code);
	
	err_code = app_timer_create(&fix_gps_timer_id,APP_TIMER_MODE_REPEATED,fix_gps_timeout_handler);
	APP_ERROR_CHECK(err_code);
	
	err_code = app_timer_create(&lora_timer_id,APP_TIMER_MODE_REPEATED,lora_timeout_handler);
	APP_ERROR_CHECK(err_code);
	
	err_code = app_timer_create(&lora_set_timer_id,APP_TIMER_MODE_REPEATED,lora_set_timeout_handler);
	APP_ERROR_CHECK(err_code);

	err_code = app_timer_create(&lora_on_timer_id,APP_TIMER_MODE_REPEATED,lora_on_timeout_handler);
	APP_ERROR_CHECK(err_code);
	
	err_code = app_timer_create(&test_accel_timer_id,APP_TIMER_MODE_REPEATED,test_accel_timeout_handler);
	APP_ERROR_CHECK(err_code);
	
	err_code = app_timer_create(&test_init_timer_id,APP_TIMER_MODE_REPEATED,test_init_timeout_handler);
	APP_ERROR_CHECK(err_code);

	err_code = app_timer_create(&test_fix_timer_id,APP_TIMER_MODE_REPEATED,test_fix_timeout_handler);
	APP_ERROR_CHECK(err_code);
}

/**@brief Function for the GAP initialization.
 *
 * @details This function will set up all the necessary GAP (Generic Access Profile) parameters of 
 *          the device. It also sets the permissions and appearance.
 */
static void gap_params_init(void)
{
    uint32_t                err_code;
    ble_gap_conn_params_t   gap_conn_params;
    ble_gap_conn_sec_mode_t sec_mode;

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);
    
    err_code = sd_ble_gap_device_name_set(&sec_mode,
                                          (const uint8_t *) DEVICE_NAME,
                                          strlen(DEVICE_NAME));
    APP_ERROR_CHECK(err_code);

    memset(&gap_conn_params, 0, sizeof(gap_conn_params));

    gap_conn_params.min_conn_interval = MIN_CONN_INTERVAL;
    gap_conn_params.max_conn_interval = MAX_CONN_INTERVAL;
    gap_conn_params.slave_latency     = SLAVE_LATENCY;
    gap_conn_params.conn_sup_timeout  = CONN_SUP_TIMEOUT;

    err_code = sd_ble_gap_ppcp_set(&gap_conn_params);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling the data from the Nordic UART Service.
 *
 * @details This function will process the data received from the Nordic UART BLE Service and send
 *          it to the UART module.
 *
 * @param[in] p_nus    Nordic UART Service structure.
 * @param[in] p_data   Data to be send to UART module.
 * @param[in] length   Length of the data.
 */
/**@snippet [Handling the data received over BLE] */
static void nus_data_handler(ble_nus_t * p_nus, uint8_t * p_data, uint16_t length)
{
    for (uint32_t i = 0; i < length; i++)
    {
        while(app_uart_put(p_data[i]) != NRF_SUCCESS);
    }
    while(app_uart_put('\r') != NRF_SUCCESS);
		if(uart_switch ==0)
			while(app_uart_put('\n') != NRF_SUCCESS);
        
    debug_print(0,"\nUART:");
    for(int i=0;i<length;i++)
        debug_print(0,"%c",p_data[i]);
    debug_print(0,"\n");
}
/**@snippet [Handling the data received over BLE] */

void setting_data_handler(ble_setting_t * p_setting, uint8_t * p_data, uint16_t length){
	
	uint32_t err_code;
	memset(noti,0,sizeof(noti));
	
	if(SET_MAJOR_MINOR == p_data[0] && 5 == length){
		
		memset(major_minor,0,sizeof(major_minor));
		
		memcpy(&major_minor[1],&p_data[1],4);
		
		storage_update(DEFAULT_DATA_BLOCK,major_minor,8,0);
		
		err_code = setting_notification_send(&m_setting,ble_success,sizeof(ble_success));
		if (err_code != NRF_ERROR_INVALID_STATE){
       APP_ERROR_CHECK(err_code);
    }
		
		memcpy(&m_beacon_info[18],&major_minor[1],4);
		advertising_init();
		
	}
	
	else if(SET_TIME_SCHEDULE == p_data[0] && 20 == length) {
		
		memset(time_schedule,0,sizeof(time_schedule));
		
		memcpy(&time_schedule[1],&p_data[1],sizeof(time_schedule)-1);
		
		storage_update(DEFAULT_DATA_BLOCK,time_schedule,20,8);
		
		err_code = setting_notification_send(&m_setting,ble_success,sizeof(ble_success));
		if (err_code != NRF_ERROR_INVALID_STATE){
       APP_ERROR_CHECK(err_code);
    }
		
	}
	
	else if(SET_GPS_GATHERING_CYCLE == p_data[0] && 2 == length){
		
		memset(gps_gathering_cycle,0,sizeof(gps_gathering_cycle));
		
		if(0 != p_data[1]){
			gps_gathering_cycle[1] = p_data[1];
			
			storage_update(DEFAULT_DATA_BLOCK,gps_gathering_cycle,4,28);
			
			gps_gathering_time = 0;

			err_code = setting_notification_send(&m_setting,ble_success,sizeof(ble_success));
			if (err_code != NRF_ERROR_INVALID_STATE){
				 APP_ERROR_CHECK(err_code);
			}
		}
		else{
			err_code = setting_notification_send(&m_setting,ble_fail,sizeof(ble_fail));
			if (err_code != NRF_ERROR_INVALID_STATE){
				 APP_ERROR_CHECK(err_code);
			}
		}
		
	}

	else if(SET_MODE == p_data[0] && 2 == length){
		// 비콘 모드, 로라 모드만 되게, 일단 다른 모드 들어오면 막고 실패 ACK 보냄
		if( 0 == p_data[1] || 1 == p_data[1]){ 
		
			memset(network_mode,0,sizeof(network_mode));
			
			network_mode[1] = p_data[1];
			
			storage_update(DEFAULT_DATA_BLOCK,network_mode,4,32);
			
			err_code = setting_notification_send(&m_setting,ble_success,sizeof(ble_success));
			if(err_code != NRF_ERROR_INVALID_STATE){
				APP_ERROR_CHECK(err_code);
			}
			
			// 모드가 바뀔 때 mode op는 동작하지 않게
			mode_op_timer_stop();
			device_reset_flag = 1;
			
		}
		else{
			err_code = setting_notification_send(&m_setting,ble_fail,sizeof(ble_fail));
			if(err_code != NRF_ERROR_INVALID_STATE){
				APP_ERROR_CHECK(err_code);
			}
		}
		
	}
	
	else if(SET_ADVERTISING_INTERVAL == p_data[0] && 2 == length){
		
		memset(advertising_interval,0,sizeof(advertising_interval));
		
		advertising_interval[1] = p_data[1];
		
		storage_update(DEFAULT_DATA_BLOCK,advertising_interval,4,36);
		
		err_code = setting_notification_send(&m_setting,ble_success,sizeof(ble_success));
		if(err_code != NRF_ERROR_INVALID_STATE){
			APP_ERROR_CHECK(err_code);
		}
		
		advertising_init();
		
	}

	else if(SET_PACKET_HEADER == p_data[0] && 6 == length){
		
		memset(packet_header,0,sizeof(packet_header));
		
		memcpy(&packet_header[1],&p_data[1],5);
		
		storage_update(DEFAULT_DATA_BLOCK,packet_header,8,40);
		
		err_code = setting_notification_send(&m_setting,ble_success,sizeof(ble_success));
		if(err_code != NRF_ERROR_INVALID_STATE){
			APP_ERROR_CHECK(err_code);
		}
		
	}
	
	else if(SET_GPS_FIX_TRY_TIME == p_data[0] && 2 == length){
		
		memset(gps_fix_try_time,0,sizeof(gps_fix_try_time));
		
		memcpy(&gps_fix_try_time[1],&p_data[1],1);
		
		storage_update(DEFAULT_DATA_BLOCK,gps_fix_try_time,4,48);
		
		gps_fix_second = 0;
		
		err_code = setting_notification_send(&m_setting,ble_success,sizeof(ble_success));
		if(err_code != NRF_ERROR_INVALID_STATE){
			APP_ERROR_CHECK(err_code);
		}
		
	}
	
	else if(SET_SOS_CLEAR_TIME == p_data[0] && 2 == length){
		
		memset(sos_clear_time,0,sizeof(sos_clear_time));
		
		memcpy(&sos_clear_time[1],&p_data[1],1);
		
		storage_update(DEFAULT_DATA_BLOCK,sos_clear_time,4,60);
		
		if(sos_time >= sos_clear_time[1]*60){
			sos_time = 0;
			m_beacon_info[21] &= ~0x01;
		
			advertising_init();
		}
		
		err_code = setting_notification_send(&m_setting,ble_success,sizeof(ble_success));
		if(err_code != NRF_ERROR_INVALID_STATE){
			APP_ERROR_CHECK(err_code);
		}
		
	}
	
	else if(SET_HEART_BEAT_CYCLE == p_data[0] && 5 == length){
	
		if(0 == p_data[1] && 0 == p_data[2] && 0 == p_data[3] && 10 > p_data[4]){
			p_data[4] = 10;
		}
		
		memset(heart_beat_cycle,0,sizeof(heart_beat_cycle));
		
		memcpy(&heart_beat_cycle[1],&p_data[1],4);
		
		memcpy(&heart_beat_cycle_time,&heart_beat_cycle[1],4);
		
		heart_beat_cycle_time = little_to_big32(heart_beat_cycle_time);
		heart_beat_time = 0;
		
		storage_update(DEFAULT_DATA_BLOCK,heart_beat_cycle,8,52);
		
		err_code = setting_notification_send(&m_setting,ble_success,sizeof(ble_success));
		if(err_code != NRF_ERROR_INVALID_STATE){
			APP_ERROR_CHECK(err_code);
		}
	}
	
	else if(SET_GYRO_IGNORE == p_data[0] && 2 == length){
		
		if(0 == p_data[1] || 1 == p_data[1]){
		
			memset(gyro_ignore,0,sizeof(gyro_ignore));
			
			memcpy(&gyro_ignore[1],&p_data[1],1);
			
			storage_update(DEFAULT_DATA_BLOCK,gyro_ignore,4,84);
			
			err_code = setting_notification_send(&m_setting,ble_success,sizeof(ble_success));
			if(err_code != NRF_ERROR_INVALID_STATE){
				APP_ERROR_CHECK(err_code);
			}
		
		}
		else{
			
			err_code = setting_notification_send(&m_setting,ble_fail,sizeof(ble_fail));
			if(err_code != NRF_ERROR_INVALID_STATE){
				APP_ERROR_CHECK(err_code);
			}
			
		}
	}

	else if(GET_MAJOR_MINOR == p_data[0] && 1 == length){
		
		noti[0] = GET_MAJOR_MINOR;
		memcpy(&noti[1],&major_minor[1],4);
		err_code = setting_notification_send(&m_setting,noti,5);
		if (err_code != NRF_ERROR_INVALID_STATE){
       APP_ERROR_CHECK(err_code);
    }
		
	}
	
	else if(GET_TIME_SCHEDULE == p_data[0] && 1 == length){
		noti[0] = GET_TIME_SCHEDULE;
		memcpy(&noti[1],&time_schedule[1],19);
		err_code = setting_notification_send(&m_setting,noti,20);
		if(err_code != NRF_ERROR_INVALID_STATE){
			APP_ERROR_CHECK(err_code);
		}
		
	}
	
	else if(GET_GPS_GATHERING_CYCLE == p_data[0] && 1 == length){
		noti[0] = GET_GPS_GATHERING_CYCLE;
		memcpy(&noti[1],&gps_gathering_cycle[1],1);		
		err_code = setting_notification_send(&m_setting,noti,2);
		if(err_code != NRF_ERROR_INVALID_STATE){
			APP_ERROR_CHECK(err_code);
		}
		
	}

	else if(GET_MODE == p_data[0] && 1 == length){
		noti[0] = GET_MODE;
		memcpy(&noti[1],&network_mode[1],1);	
		err_code = setting_notification_send(&m_setting,noti,2);
		if(err_code != NRF_ERROR_INVALID_STATE){
			APP_ERROR_CHECK(err_code);
		}
		
	}
	
	else if(GET_ADVERTISING_INTERVAL == p_data[0] && 1 == length){
		noti[0] = GET_ADVERTISING_INTERVAL;
		memcpy(&noti[1],&advertising_interval[1],1);
		err_code = setting_notification_send(&m_setting,noti,2);
		if(err_code != NRF_ERROR_INVALID_STATE){
			APP_ERROR_CHECK(err_code);
		}
		
	}
	
	else if(GET_PACKET_HEADER == p_data[0] && 1 == length){
		noti[0] = GET_PACKET_HEADER;
		memcpy(&noti[1],&packet_header[1],5);
		err_code = setting_notification_send(&m_setting,noti,6);
		if(err_code != NRF_ERROR_INVALID_STATE){
			APP_ERROR_CHECK(err_code);
		}
		
	}
	
	else if(GET_GPS_FIX_TRY_TIME == p_data[0] && 1 == length){
		noti[0] = GET_GPS_FIX_TRY_TIME;
		memcpy(&noti[1],&gps_fix_try_time[1],1);
		err_code = setting_notification_send(&m_setting,noti,2);
		if(err_code != NRF_ERROR_INVALID_STATE){
			APP_ERROR_CHECK(err_code);
		}
	}
	
	else if(GET_LORA_EUI == p_data[0] && 1 == length){
		noti[0] = GET_LORA_EUI;
		memcpy(&noti[1],&lora_eui,8);
		err_code = setting_notification_send(&m_setting,noti,9);
		if(err_code != NRF_ERROR_INVALID_STATE){
			APP_ERROR_CHECK(err_code);
		}
	}
	
	else if(GET_CURRENT_TIME == p_data[0] && 1 == length){
		current_time[0] = time.hour;
		current_time[1] = time.minute;
		current_time[2] = time.second;
		noti[0] = GET_CURRENT_TIME;
		memcpy(&noti[1],current_time,3);
		err_code = setting_notification_send(&m_setting,noti,4);
		if (err_code != NRF_ERROR_INVALID_STATE){
			APP_ERROR_CHECK(err_code);
		}
	}
	
	else if(GET_FIRMWARE_VERSION == p_data[0] && 1 == length){
		noti[0] = GET_FIRMWARE_VERSION;
		memcpy(&noti[1],firmware_version,3);
		err_code = setting_notification_send(&m_setting,noti,4);
		if (err_code != NRF_ERROR_INVALID_STATE){
			APP_ERROR_CHECK(err_code);
		}
	}
	
	else if(GET_SOS_CLEAR_TIME == p_data[0] && 1 == length){
		noti[0] = GET_SOS_CLEAR_TIME;
		memcpy(&noti[1],&sos_clear_time[1],1);
		err_code = setting_notification_send(&m_setting,noti,2);
		if(err_code != NRF_ERROR_INVALID_STATE){
			APP_ERROR_CHECK(err_code);
		}
	}
	
	else if(GET_HEART_BEAT_CYCLE == p_data[0] && 1 == length){
		noti[0] = GET_HEART_BEAT_CYCLE;
		memcpy(&noti[1],&heart_beat_cycle[1],4);
		err_code = setting_notification_send(&m_setting,noti,5);
		if (err_code != NRF_ERROR_INVALID_STATE){
			APP_ERROR_CHECK(err_code);
		}
	}
	
	else if(GET_GYRO_IGNORE == p_data[0] && 1 == length){
		noti[0] = GET_GYRO_IGNORE;
		memcpy(&noti[1],&gyro_ignore[1],1);
		err_code = setting_notification_send(&m_setting,noti,2);
		if(err_code != NRF_ERROR_INVALID_STATE){
			APP_ERROR_CHECK(err_code);
		}
	}
	
	else if(GET_LORA_APP_KEY == p_data[0] && 1 == length){
		noti[0] = GET_LORA_APP_KEY;
		memcpy(&noti[1],lora_app_key,16);
		err_code = setting_notification_send(&m_setting,noti,17);
		if (err_code != NRF_ERROR_INVALID_STATE){
			APP_ERROR_CHECK(err_code);
		}
	}
	
	//양산할 때 Major / Minor, App EUI, Device EUI, App Key를 받아 설정
	else if(FACTORY_SETTING == p_data[0] && 37 == length){
		
		uint8_t app_eui[8],device_eui[8],app_key[16];
		char at_ai[25] = "AT+AI ",at_di[25] = "AT+DI ",at_pak[45] = "AT+PAK ";
		char s_app_eui[20],s_device_eui[20],s_app_key[35];
		
		memcpy(app_eui,&p_data[1],8);
		memcpy(device_eui,&p_data[9],8);
		memcpy(app_key,&p_data[17],16);
		
		// Application EUI, Device EUI, Application Key, Major / Minor 중 하나라도 모두 0이 들어오면 Fail ACK
		if(1 == eui_check(app_eui) && 1 == eui_check(device_eui) && 1 == app_key_check(app_key) && (0 == p_data[33] && 0 == p_data[34] && 0 == p_data[35] && 0 == p_data[36])){
			err_code = setting_notification_send(&m_setting,ble_fail,sizeof(ble_fail));
			if(err_code != NRF_ERROR_INVALID_STATE){
				APP_ERROR_CHECK(err_code);
			}
		}
		// 모든 값이 제대로 들어왔다면 설정하기
		else{	
			memset(major_minor,0,sizeof(major_minor));
			memcpy(&major_minor[1],&p_data[33],4);
			
			debug_print(0,"Major / Minor : %02x %02x %02x %02x\n",p_data[33],p_data[34],p_data[35],p_data[36]);
			
			storage_update(DEFAULT_DATA_BLOCK,major_minor,8,0);
			
			memset(s_app_eui,0,sizeof(s_app_eui));
			for(int i=0;i<8;i++)
				sprintf(&s_app_eui[strlen(s_app_eui)],"%02x",app_eui[i]);
			strcat(at_ai,s_app_eui);
			
			memset(command,0,sizeof(command));
			command[0] = strlen(at_ai);
			strcpy(&command[1],at_ai);
			command_enqueue();

			memset(s_device_eui,0,sizeof(s_device_eui));
			for(int i=0;i<8;i++)
				sprintf(&s_device_eui[strlen(s_device_eui)],"%02x",device_eui[i]);
			strcat(at_di,s_device_eui);

			memset(command,0,sizeof(command));
			command[0] = strlen(at_di);
			strcpy(&command[1],at_di);
			command_enqueue();

			memset(s_app_key,0,sizeof(s_app_key));
			for(int i=0;i<16;i++)
				sprintf(&s_app_key[strlen(s_app_key)],"%02x",app_key[i]);
			strcat(at_pak,s_app_key);

			memset(command,0,sizeof(command));
			command[0] = strlen(at_pak);
			strcpy(&command[1],at_pak);
			command_enqueue();
			
			memset(command,0,sizeof(command));
			command[0] = strlen(lora_save);
			strcpy(&command[1],lora_save);
			command_enqueue();
			
			factory_setting_flag = 1;
			
			err_code = setting_notification_send(&m_setting,ble_success,sizeof(ble_success));
			if(err_code != NRF_ERROR_INVALID_STATE){
				APP_ERROR_CHECK(err_code);
			}
		}
	}
	
	else{
		err_code = setting_notification_send(&m_setting,ble_fail,sizeof(ble_fail));
		if (err_code != NRF_ERROR_INVALID_STATE){
			APP_ERROR_CHECK(err_code);
		}
	}
}
#ifdef AT_COMMAND
void lora_data_handler(ble_lora_t * p_lora, uint8_t * p_data, uint16_t length){
	
		debug_print(0,"LORA UART\n");
		
		uart_switch = 1;
	
		memset(command,0,sizeof(command));
	
		command[0] = (uint8_t)length;
		memcpy(&command[1],p_data,(uint8_t)length);
	
		command_enqueue();
		
}
#endif
/**@brief Function for initializing services that will be used by the application.
 */
static void services_init(void)
{
    uint32_t       err_code;
    ble_nus_init_t nus_init;
    ble_dfu_init_t   dfus_init;
    ble_setting_init_t setting_init;
#ifdef AT_COMMAND
    ble_lora_init_t lora_init;
#endif
    ble_bas_init_t bas_init;

    // Initialize Battery Service.
    memset(&bas_init, 0, sizeof(bas_init));

    // Here the sec level for the Battery Service can be changed/increased.
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&bas_init.battery_level_char_attr_md.cccd_write_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&bas_init.battery_level_char_attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_NO_ACCESS(&bas_init.battery_level_char_attr_md.write_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&bas_init.battery_level_report_read_perm);

    bas_init.evt_handler          = NULL;
    bas_init.support_notification = true;
    bas_init.p_report_ref         = NULL;
    bas_init.initial_batt_level   = 100;

    err_code = ble_bas_init(&m_bas, &bas_init);
    APP_ERROR_CHECK(err_code);

    // Initialize Nordic UART Service.
    memset(&nus_init, 0, sizeof(nus_init));
    nus_init.data_handler = nus_data_handler;
    err_code = ble_nus_init(&m_nus, &nus_init);
    APP_ERROR_CHECK(err_code);

    // Initialize the Device Firmware Update Service.
    memset(&dfus_init, 0, sizeof(dfus_init));

    dfus_init.evt_handler   = dfu_app_on_dfu_evt;
    dfus_init.error_handler = NULL;
    dfus_init.revision      = DFU_REVISION;

    err_code = ble_dfu_init(&m_dfus, &dfus_init);
    APP_ERROR_CHECK(err_code);

    dfu_app_reset_prepare_set(reset_prepare);
    dfu_app_dm_appl_instance_set(m_app_handle);

    // Initialize BLE Setting Service
    memset(&setting_init, 0, sizeof(setting_init));
    setting_init.data_handler = setting_data_handler;
    err_code = ble_setting_init(&m_setting, &setting_init);
    APP_ERROR_CHECK(err_code);
    
#ifdef AT_COMMAND	
    // Initialize LoRa Setting Service
    memset(&lora_init, 0, sizeof(lora_init));

    lora_init.data_handler = lora_data_handler;
    err_code = ble_lora_init(&m_lora, &lora_init);
    APP_ERROR_CHECK(err_code);
#endif
}


/**@brief Function for handling an event from the Connection Parameters Module.
 *
 * @details This function will be called for all events in the Connection Parameters Module
 *          which are passed to the application.
 *
 * @note All this function does is to disconnect. This could have been done by simply setting
 *       the disconnect_on_fail config parameter, but instead we use the event handler
 *       mechanism to demonstrate its use.
 *
 * @param[in] p_evt  Event received from the Connection Parameters Module.
 */
static void on_conn_params_evt(ble_conn_params_evt_t * p_evt)
{
    uint32_t err_code;
    
    if(p_evt->evt_type == BLE_CONN_PARAMS_EVT_FAILED)
    {
        err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_CONN_INTERVAL_UNACCEPTABLE);
        APP_ERROR_CHECK(err_code);
    }
}


/**@brief Function for handling errors from the Connection Parameters module.
 *
 * @param[in] nrf_error  Error code containing information about what went wrong.
 */
static void conn_params_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}


/**@brief Function for initializing the Connection Parameters module.
 */
static void conn_params_init(void)
{
    uint32_t               err_code;
    ble_conn_params_init_t cp_init;
    
    memset(&cp_init, 0, sizeof(cp_init));

    cp_init.p_conn_params                  = NULL;
    cp_init.first_conn_params_update_delay = FIRST_CONN_PARAMS_UPDATE_DELAY;
    cp_init.next_conn_params_update_delay  = NEXT_CONN_PARAMS_UPDATE_DELAY;
    cp_init.max_conn_params_update_count   = MAX_CONN_PARAMS_UPDATE_COUNT;
    cp_init.start_on_notify_cccd_handle    = BLE_GATT_HANDLE_INVALID;
    cp_init.disconnect_on_fail             = false;
    cp_init.evt_handler                    = on_conn_params_evt;
    cp_init.error_handler                  = conn_params_error_handler;
    
    err_code = ble_conn_params_init(&cp_init);
    APP_ERROR_CHECK(err_code);
}

static void battery_level_update(void)
{
    uint32_t err_code;

    battery_state = batt_adc_check();

    char sstr[45] = {0,};
    sprintf(sstr, "batt:%d", battery_state);
    ble_nus_string_send(&m_nus,(uint8_t *)sstr,strlen(sstr));

    // 배터리가 0%로 20초 동안 유지되면 Device Off
    if(0 == battery_state){
        battery_cut_off_cnt++;
        debug_print(0,"Battery Cut Off cnt : %d\n",battery_cut_off_cnt);
        if(20 == battery_cut_off_cnt){
            debug_print(0,"Battery Cut Off, Device Off\n");
            clear_led();
            power_off_flag = 1;
            led_state = 2;
        }
    }
    // 배터리에 따라 Major / Minor 비트 변경
    else{
        
        battery_cut_off_cnt = 0;
        
        if(75 > battery_state){
            m_beacon_info[21] &= ~(0x03 << 1);
            m_beacon_info[21] |= (0x03 << 1);
        }
        else if(75 <= battery_state && 80 > battery_state){
            m_beacon_info[21] &= ~(0x03 << 1);
            m_beacon_info[21] |= (0x01 << 2);
        }
        else if(80 <= battery_state && 85 > battery_state){
            m_beacon_info[21] &= ~(0x03 << 1);
            m_beacon_info[21] |= (0x01 << 1);
        }
        else if(85 <= battery_state){
            m_beacon_info[21] &= ~(0x03 << 1);
        }
    }

    err_code = ble_bas_battery_level_update(&m_bas, battery_state);
    if ((err_code != NRF_SUCCESS) &&
        (err_code != NRF_ERROR_INVALID_STATE) &&
        (err_code != BLE_ERROR_NO_TX_PACKETS) &&
        (err_code != BLE_ERROR_GATTS_SYS_ATTR_MISSING)
    )
    {
        APP_ERROR_HANDLER(err_code);
    }
		
}


/**@brief Function for the application's SoftDevice event handler.
 *
 * @param[in] p_ble_evt SoftDevice event.
 */
static void on_ble_evt(ble_evt_t * p_ble_evt)
{
    uint32_t                         err_code;
    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_CONNECTED:
			//debug_print(0,"mjlee Connected\n");
            debug_print(0,"Connected\n");
            m_conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
            break;
            
        case BLE_GAP_EVT_DISCONNECTED:
            //debug_print(0,"mjlee Disconnected\n");
            debug_print(0,"Disconnected\n");
            m_conn_handle = BLE_CONN_HANDLE_INVALID;
            if(1 == test_power_flag){
                advertising_init();
                advertising_start();
            }
            else{
                if(0 == network_mode[1]){
                    clear_led();
                    led_state = 4;
                    advertising_flag = 0;
                }
                else if(1 == network_mode[1]){
                    if(1 == fixed_uuid_flag){
                        clear_led();
                        led_state = 4;
                        fixed_uuid_flag = 0;
                    }
                    advertising_flag = 1;
                    advertising_init();
                    advertising_start();
                }
            }
            break;

        case BLE_GAP_EVT_TIMEOUT:
            //debug_print(0,"mjlee Advertising Timeout\n");
            debug_print(0,"Advertising Timeout\n");
            if(0 == network_mode[1]){
                clear_led();
                led_state = 4;
                advertising_flag = 0;
            }
            else if(1 == network_mode[1]){
                if(1 == fixed_uuid_flag){
                    fixed_uuid_flag = 0;
                }
                advertising_flag = 1;				
                advertising_init();
                advertising_start();
            }
            break;
				
        case BLE_GAP_EVT_SEC_PARAMS_REQUEST:
			//debug_print(0,"mjlee Sec Request\n");
            debug_print(0,"Sec Request\n");
            // Pairing not supported
            err_code = sd_ble_gap_sec_params_reply(m_conn_handle, BLE_GAP_SEC_STATUS_PAIRING_NOT_SUPP, NULL, NULL);
            APP_ERROR_CHECK(err_code);
            break;

        case BLE_GATTS_EVT_SYS_ATTR_MISSING:
			//debug_print(0,"mjlee Sys Attr Missing\n");
            debug_print(0,"Sys Attr Missing\n");
            // No system attributes have been stored.
            err_code = sd_ble_gatts_sys_attr_set(m_conn_handle, NULL, 0, 0);
            APP_ERROR_CHECK(err_code);
            break;

		case BLE_EVT_USER_MEM_REQUEST:
			//debug_print(0,"mjlee User mem request\n");
            debug_print(0,"User mem request\n");
            mem_block.len = QUEUED_WRITE_BUFFER_SIZE;
            mem_block.p_mem = &queued_write_buffer[0];
            err_code = sd_ble_user_mem_reply(m_conn_handle, &mem_block);
			APP_ERROR_CHECK(err_code);
            break;
				
        case BLE_EVT_USER_MEM_RELEASE:			
            if ((p_ble_evt->evt.common_evt.params.user_mem_release.mem_block.p_mem == mem_block.p_mem)&&(p_ble_evt->evt.common_evt.params.user_mem_release.mem_block.len == mem_block.len))
            {
                //memory released do nothing. 
                //debug_print(0,"mjlee User mem released\n");
                debug_print(0,"User mem released\n");
                // memory block, Queue 초기화
                mem_block.p_mem = NULL;
                mem_block.len = 0;
                memset(queued_write_buffer,0,sizeof(queued_write_buffer));
            }
			break;
						
        case BLE_GATTS_EVT_WRITE:
            // memory block이 있을때 buffer를 만들어 저장한 후, 맞는 위치의 핸들러로 보냄
            if(mem_block.p_mem != NULL){ 
                uint8_t buffer_data[350];
                uint8_t length = 0;
                uint8_t index = 0;
                while(mem_block.p_mem[index+4]){
                    memcpy(&buffer_data[length],&mem_block.p_mem[index+6],mem_block.p_mem[index+4]);

                    length += mem_block.p_mem[index+4];
                    index += mem_block.p_mem[index+4] + 6;
                }
        
                if(*((uint16_t *)mem_block.p_mem) == m_setting.write_handle.value_handle){
                    setting_data_handler(&m_setting,buffer_data,length);
                }
#ifdef AT_COMMAND
                else if(*((uint16_t *)mem_block.p_mem) == m_lora.write_handle.value_handle){
                    lora_data_handler(&m_lora,buffer_data,length);
                }
#endif
            }
            break;
					
        default:
            // No implementation needed.
            break;
    }
}


/**@brief Function for dispatching a SoftDevice event to all modules with a SoftDevice 
 *        event handler.
 *
 * @details This function is called from the SoftDevice event interrupt handler after a 
 *          SoftDevice event has been received.
 *
 * @param[in] p_ble_evt  SoftDevice event.
 */
static void ble_evt_dispatch(ble_evt_t * p_ble_evt)
{
    dm_ble_evt_handler(p_ble_evt);
    ble_conn_params_on_ble_evt(p_ble_evt);
    ble_bas_on_ble_evt(&m_bas, p_ble_evt);
    ble_nus_on_ble_evt(&m_nus, p_ble_evt);
    ble_dfu_on_ble_evt(&m_dfus, p_ble_evt);
    on_ble_evt(p_ble_evt);
    ble_setting_on_ble_evt(&m_setting,p_ble_evt);
#ifdef AT_COMMAND
    ble_lora_on_ble_evt(&m_lora,p_ble_evt);
#endif
}

static void sys_evt_dispatch(uint32_t sys_evt)
{
		pstorage_sys_event_handler(sys_evt);
}

/**@brief Function for the SoftDevice initialization.
 *
 * @details This function initializes the SoftDevice and the BLE event interrupt.
 */
static void ble_stack_init(void)
{
    uint32_t err_code;

    nrf_clock_lf_cfg_t clock_lf_cfg = NRF_CLOCK_LFCLKSRC;

    // Initialize SoftDevice.
    SOFTDEVICE_HANDLER_INIT(&clock_lf_cfg, NULL);

    ble_enable_params_t ble_enable_params;
    err_code = softdevice_enable_get_default_config(CENTRAL_LINK_COUNT,
                                                    PERIPHERAL_LINK_COUNT,
                                                    &ble_enable_params);
    APP_ERROR_CHECK(err_code);

    ble_enable_params.gatts_enable_params.service_changed = 1;	
    ble_enable_params.common_enable_params.vs_uuid_count = 5;

    //Check the ram settings against the used number of links
    CHECK_RAM_START_ADDR(CENTRAL_LINK_COUNT,PERIPHERAL_LINK_COUNT);
    
    // Enable BLE stack.
    err_code = softdevice_enable(&ble_enable_params);
    APP_ERROR_CHECK(err_code);

    // Subscribe for BLE events.
    err_code = softdevice_ble_evt_handler_set(ble_evt_dispatch);
    APP_ERROR_CHECK(err_code);
    err_code = softdevice_sys_evt_handler_set(sys_evt_dispatch);
    APP_ERROR_CHECK(err_code);
}

/**@brief   Function for handling app_uart events.
 *
 * @details This function will receive a single character from the app_uart module and append it to 
 *          a string. The string will be be sent over BLE when the last character received was a 
 *          'new line' i.e '\n' (hex 0x0D) or if the string has reached a length of 
 *          @ref NUS_MAX_DATA_LENGTH.
 */
/**@snippet [Handling the data received over UART] */
void uart_event_handle(app_uart_evt_t * p_event)
{
    static uint8_t data_array[BLE_NUS_MAX_DATA_LEN];
    static uint8_t index = 0;
    uint32_t       err_code;
    switch (p_event->evt_type)
    {
        case APP_UART_DATA_READY:
            UNUSED_VARIABLE(app_uart_get(&data_array[index]));
            index++;
            if ((data_array[index - 1] == '\n') || (data_array[index - 1] == 0xff) || (index >= (BLE_NUS_MAX_DATA_LEN))){
                memcpy(&receive_data[p_index],data_array,index);
                p_index += index;

                if(data_array[index-1] == '\n' || data_array[index-1] == 0xff){
                    debug_print(0,"UarT: %s\n",receive_data);
                    if(0 == uart_switch){
                        if(1 == test_power_flag){
                            test_gps_parsing(receive_data,p_index);
                        }
                        else{
                            if(1 == gps_setting_flag){
                                gps_parsing(receive_data,p_index);
                            }
                        }
                    }
                    else{
                        if(1 == test_power_flag){
                            test_lora_parsing(receive_data,p_index);
                        }
                        else{
                            lora_parsing(receive_data,p_index);
                        }
                    }
                    p_index = 0;
                    memset(receive_data,0,sizeof(receive_data));
                }
                
                // UART 데이터를 notification으로 보냄								
                err_code = ble_nus_string_send(&m_nus, data_array, index);
                if (err_code != NRF_ERROR_INVALID_STATE && 
                    err_code != BLE_ERROR_GATTS_SYS_ATTR_MISSING &&
                    err_code != BLE_ERROR_NO_TX_PACKETS)
                {
                    APP_ERROR_CHECK(err_code);
                }
                else{
                    if(err_code == BLE_ERROR_GATTS_SYS_ATTR_MISSING){
                        debug_print(0,"BLE ERROR GATTS SYS ATTR MISSING\n");
                        err_code = sd_ble_gatts_sys_attr_set(m_conn_handle,NULL,0,0);
                        APP_ERROR_CHECK(err_code);
                    }
                    else if(err_code == BLE_ERROR_NO_TX_PACKETS){
                        debug_print(0,"BLE ERROR NO TX PACKETS\n");
                    }
                    }
                index = 0;
            }
            break;

        case APP_UART_COMMUNICATION_ERROR:
            debug_print(0,"UART Communication Error, %d\n",p_event->data.error_communication);
            uart_stop();
            uart_start();
            memset(data_array,0,sizeof(data_array));
            memset(receive_data,0,sizeof(receive_data));
            index = 0;
            p_index = 0;
            //APP_ERROR_HANDLER(p_event->data.error_communication);
            break;

        case APP_UART_FIFO_ERROR:
            debug_print(0,"UART FIFO Error\n");
            APP_ERROR_HANDLER(p_event->data.error_code);
            break;
				
        case APP_UART_TX_EMPTY:
            //debug_print(0,"UART TX Empty\n");
            /*
            if(1 == lora_setting_flag){
                lora_setting_cnt++;
                if(2 == lora_setting_cnt){
                    lora_setting_flag = 0;
                    lora_setting_cnt = 0;
                    uart_stop();
                }
            }
            */
			break;
				
        default:
            break;
    }
}
/**@snippet [Handling the data received over UART] */

/**@brief  Function for initializing the UART module.
 */
/**@snippet [UART Initialization] */
static void uart_start(void)
{
    uint32_t err_code;
    uint32_t rx_pin, tx_pin, baudrate;

    debug_print(0,"uart_start %d\n", uart_switch);

    if(0 == uart_switch){
        //rx_pin = GPIO_GPS_RX_PIN;
        //tx_pin = GPIO_GPS_TX_PIN;
        rx_pin = GPIO_GPS_TX_PIN;
        tx_pin = GPIO_GPS_RX_PIN;
        baudrate = UART_BAUDRATE_BAUDRATE_Baud9600;
        gps_uart_flag = 1;
    }
    else{
        rx_pin = GPIO_LORA_RX_PIN;
        tx_pin = GPIO_LORA_TX_PIN;
        if(0 == test_lora_flag){
            if(1 == lora_setting_flag){
                baudrate = UART_BAUDRATE_BAUDRATE_Baud115200;
            }
            else{
                baudrate = UART_BAUDRATE_BAUDRATE_Baud9600;
            }
        }
        else{
            baudrate = UART_BAUDRATE_BAUDRATE_Baud115200;
        }
        lora_uart_flag = 1;
    }

    const app_uart_comm_params_t comm_params =
    {
        rx_pin,
        tx_pin,
        NULL,
        NULL,
        APP_UART_FLOW_CONTROL_DISABLED,
        false,
        baudrate
    };

    APP_UART_FIFO_INIT( &comm_params,
                       UART_RX_BUF_SIZE,
                       UART_TX_BUF_SIZE,
                       uart_event_handle,
                       APP_IRQ_PRIORITY_LOW,
                       err_code);
    APP_ERROR_CHECK(err_code);
		
}
/**@snippet [UART Initialization] */


void uart_stop(void){
	uint32_t err_code;
	
	gps_uart_flag = 0;
	lora_uart_flag = 0;
	
	err_code = app_uart_close();
	APP_ERROR_CHECK(err_code);
	
	err_code = app_uart_flush();
	APP_ERROR_CHECK(err_code);
	
	debug_print(0,"uart_stop %d\n", uart_switch);
}

/**@brief Function for handling the Device Manager events.
 *
 * @param[in] p_evt  Data associated to the device manager event.
 */
static uint32_t device_manager_evt_handler(dm_handle_t const * p_handle,
                                           dm_event_t const  * p_event,
                                           ret_code_t        event_result)
{
    APP_ERROR_CHECK(event_result);

    if (p_event->event_id == DM_EVT_LINK_SECURED)
    {
        app_context_load(p_handle);
    }

    return NRF_SUCCESS;
}


/**@brief Function for the Device Manager initialization.
 *
 * @param[in] erase_bonds  Indicates whether bonding information should be cleared from
 *                         persistent storage during initialization of the Device Manager.
 */
static void device_manager_init()
{
    uint32_t               err_code;
    dm_init_param_t        init_param = {.clear_persistent_data = NULL};
    dm_application_param_t register_param;

    // Initialize persistent storage module.
    err_code = pstorage_init();
    APP_ERROR_CHECK(err_code);

    err_code = dm_init(&init_param);
    APP_ERROR_CHECK(err_code);
		
    memset(&register_param.sec_param, 0, sizeof(ble_gap_sec_params_t));

    register_param.sec_param.bond         = SEC_PARAM_BOND;
    register_param.sec_param.mitm         = SEC_PARAM_MITM;
    register_param.sec_param.lesc         = SEC_PARAM_LESC;
    register_param.sec_param.keypress     = SEC_PARAM_KEYPRESS;
    register_param.sec_param.io_caps      = SEC_PARAM_IO_CAPABILITIES;
    register_param.sec_param.oob          = SEC_PARAM_OOB;
    register_param.sec_param.min_key_size = SEC_PARAM_MIN_KEY_SIZE;
    register_param.sec_param.max_key_size = SEC_PARAM_MAX_KEY_SIZE;
    register_param.evt_handler            = device_manager_evt_handler;
    register_param.service_type           = DM_PROTOCOL_CNTXT_GATT_SRVR_ID;

    err_code = dm_register(&m_app_handle, &register_param);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for initializing the Advertising functionality.
 */
void advertising_init(void){
    uint32_t      err_code;
    ble_advdata_t advdata;
    ble_advdata_t scanrsp;

    ble_advdata_manuf_data_t manuf_specific_data;

    manuf_specific_data.company_identifier = APP_COMPANY_IDENTIFIER;

    manuf_specific_data.data.p_data = (uint8_t *) m_beacon_info;
    manuf_specific_data.data.size   = APP_BEACON_INFO_LENGTH;

    // Build and set advertising data.
    memset(&advdata, 0, sizeof(advdata));

    advdata.flags                 = BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE;
    advdata.p_manuf_specific_data = &manuf_specific_data;

    memset(&scanrsp, 0, sizeof(scanrsp));
    scanrsp.name_type             = BLE_ADVDATA_FULL_NAME;

    err_code = ble_advdata_set(&advdata, &scanrsp);
    APP_ERROR_CHECK(err_code);
}

void advertising_start(void){
    uint32_t err_code;

    memset(&m_adv_params, 0, sizeof(m_adv_params));

    m_adv_params.type        = BLE_GAP_ADV_TYPE_ADV_IND;
    m_adv_params.p_peer_addr = NULL;                             
    m_adv_params.fp          = BLE_GAP_ADV_FP_ANY;
    m_adv_params.interval    = advertising_interval[1]*1600;

    if(1 == test_power_flag){
        m_adv_params.timeout = APP_BEACON_NON_TIMEOUT; 
    }
    else{
        
        if(1 == app_key_check_flag){
            m_adv_params.timeout =	APP_BEACON_NON_TIMEOUT;
        }
        else{
            // 로라 모드
            if(0 == network_mode[1]){
                m_adv_params.timeout = APP_BEACON_TIMEOUT_IN_SECONDS;
            }
            // 비콘 모드
            else if(1 == network_mode[1]){
                if(0 == fixed_uuid_flag){
                    m_adv_params.timeout = APP_BEACON_NON_TIMEOUT;
                }
                else{
                    m_adv_params.timeout = APP_BEACON_TIMEOUT_IN_SECONDS;
                }
            }
        }
    }
    err_code = sd_ble_gap_adv_start(&m_adv_params);
    APP_ERROR_CHECK(err_code);
}

/**@brief Function for placing the application in low power state while waiting for events.
 */
void power_manage(void)
{
    uint32_t err_code = sd_app_evt_wait();
    APP_ERROR_CHECK(err_code);
}

static void gpio_init(void){
	
	nrf_gpio_cfg_input(GPIO_POWER_BUTTON_PIN,NRF_GPIO_PIN_NOPULL);
	nrf_gpio_cfg_input(GPIO_KEY_PIN,NRF_GPIO_PIN_NOPULL);
	
	nrf_gpio_cfg_input(GPIO_CHARGE_PIN,NRF_GPIO_PIN_NOPULL);
	nrf_gpio_cfg_input(GPIO_USB_PIN,NRF_GPIO_PIN_NOPULL);
	
	nrf_gpio_cfg_output(GPIO_POWER_ON_PIN);
	nrf_gpio_cfg_output(GPIO_STATUS_RED_LED);
	nrf_gpio_cfg_output(GPIO_STATUS_BLUE_LED);
	nrf_gpio_cfg_output(GPIO_LORA_PWR_PIN);
	nrf_gpio_cfg_output(GPIO_LORA_INTP_PIN);
	nrf_gpio_cfg_output(GPIO_GPS_V_PWR_PIN);
	nrf_gpio_cfg_output(GPIO_GPS_PWR_PIN);

    nrf_gpio_cfg_output(GPIO_GPS_ON_OFF);
    nrf_gpio_cfg_output(GPIO_GPS_SYSTEM_ON);
    nrf_gpio_cfg_output(GPIO_GPS_RESET);
    
	
	nrf_gpio_pin_set(GPIO_STATUS_RED_LED);
	nrf_gpio_pin_set(GPIO_STATUS_BLUE_LED);
	nrf_gpio_pin_clear(GPIO_LORA_PWR_PIN);
	nrf_gpio_pin_clear(GPIO_LORA_INTP_PIN);
	nrf_gpio_pin_clear(GPIO_GPS_V_PWR_PIN);
	nrf_gpio_pin_clear(GPIO_GPS_PWR_PIN);

    nrf_gpio_pin_clear(GPIO_GPS_ON_OFF);
    nrf_gpio_pin_clear(GPIO_GPS_SYSTEM_ON);
	nrf_gpio_pin_clear(GPIO_GPS_RESET);
	nrf_gpio_pin_set(GPIO_GPS_RESET);
}

static void spi_init(void){
	
	uint32_t err_code;
	nrf_drv_spi_config_t spi_config = {
        .sck_pin 		= GPIO_SCK_PIN,
        .mosi_pin 	    = GPIO_MOSI_PIN,
        .miso_pin 	    = GPIO_MISO_PIN,
        .ss_pin			= GPIO_SS_PIN,
        .irq_priority	= APP_IRQ_PRIORITY_LOW,
        .orc 			= 0xCC,
        .frequency	    = NRF_DRV_SPI_FREQ_1M,
        .mode			= NRF_DRV_SPI_MODE_3,
        .bit_order		= NRF_DRV_SPI_BIT_ORDER_MSB_FIRST
	};
	err_code = nrf_drv_spi_init(&spi, &spi_config, NULL);
	APP_ERROR_CHECK(err_code);
	
	bma250e_spi_init(&spi);
}

void initialize_data_load(){
	
	storage_load(DEFAULT_DATA_BLOCK,major_minor,8,0);
	if(0xff == major_minor[0]){
		memcpy(&major_minor[1],&m_beacon_info[18],4);
	}
	else{
		memcpy(&m_beacon_info[18],&major_minor[1],4);
	}
	
	storage_load(DEFAULT_DATA_BLOCK,time_schedule,20,8);
	if(0xff == time_schedule[0]){
		memset(&time_schedule[1],0xff,sizeof(time_schedule)-2);
		time_schedule[19] = 0xfe;
	}

	storage_load(DEFAULT_DATA_BLOCK,gps_gathering_cycle,4,28);
	if(0xff == gps_gathering_cycle[0])
		//gps_gathering_cycle[1] = 0x1e;
        gps_gathering_cycle[1] = 0x06;

	storage_load(DEFAULT_DATA_BLOCK,network_mode,4,32);
	if(0xff == network_mode[0])
		network_mode[1] = 0;
	
	storage_load(DEFAULT_DATA_BLOCK,advertising_interval,4,36);
	if(0xff == advertising_interval[0])
		advertising_interval[1] = 1;
	
	storage_load(DEFAULT_DATA_BLOCK,packet_header,8,40);
	if(0xff == packet_header[0])
		memset(packet_header,0,sizeof(packet_header));
	
	storage_load(DEFAULT_DATA_BLOCK,gps_fix_try_time,4,48);
	if(0xff == gps_fix_try_time[0])
		gps_fix_try_time[1] = 3;

	storage_load(DEFAULT_DATA_BLOCK,heart_beat_cycle,8,52);
	if(0xff == heart_beat_cycle[0]){
		heart_beat_cycle[1] = 0;
		heart_beat_cycle[2] = 0;
		heart_beat_cycle[3] = 0x0E;
		heart_beat_cycle[4] = 0x10;
		
		memcpy(&heart_beat_cycle_time,&heart_beat_cycle[1],4);
		heart_beat_cycle_time = little_to_big32(heart_beat_cycle_time);
	}
	else{
		memcpy(&heart_beat_cycle_time,&heart_beat_cycle[1],4);
		heart_beat_cycle_time = little_to_big32(heart_beat_cycle_time);
	}
	
	storage_load(DEFAULT_DATA_BLOCK,sos_clear_time,4,60);
	if(0xff == sos_clear_time[0]){
		sos_clear_time[1] = 30;
	}
	
	storage_load(DEFAULT_DATA_BLOCK,set_app_key,16,64);
	if(1 == app_key_check(set_app_key)){
        //mjlee, 데이터가 0
        //debug_print(0, "initialize_data_load, is_save_app_key 0 %d, %d\n", true, false);
		is_save_app_key = 0;
	}
	else{
        //mjlee, 데이터가 0이 아닐 때
        //debug_print(0, "initialize_data_load, is_save_app_key 1 %d, %d\n", true, false);
		is_save_app_key = 1;
	}

	storage_load(DEFAULT_DATA_BLOCK,lora_retranse_num,4,80);
	if(0xff == lora_retranse_num[0]){
		lora_retranse_num[1] = 3;
	}
	
	storage_load(DEFAULT_DATA_BLOCK,gyro_ignore,4,84);
	if(0xff == gyro_ignore[0]){
		gyro_ignore[1] = 0;
	}
	
	storage_load(DEFAULT_DATA_BLOCK,first_boot_flag,4,88);
	if(0xff == first_boot_flag[0]){
		first_boot_flag[1] = 1;
	}

	storage_load(DEFAULT_DATA_BLOCK,real_join_flag,4,92);
	if(0xff == real_join_flag[0]){
		real_join_flag[1] = 1;
	}
}


/**@brief Application main function.
 */

int main(void)
{
    // Initialize.
    gpio_init();
    spi_init();
    timers_init();
    ble_stack_init();
    device_manager_init();
    storage_init(BLOCK_SIZE,BLOCK_COUNT);
    initialize_data_load();
    gap_params_init();
    services_init();
    conn_params_init();
		
    // Device Reset이 될 때 바로 켜지도록
    if(NRF_POWER->GPREGRET == 10){        
        debug_print(0,"Device Reset\n");
        
        NRF_POWER->GPREGRET = 0;
        
        nrf_gpio_pin_set(GPIO_POWER_ON_PIN);
        nrf_gpio_pin_set(GPIO_GPS_V_PWR_PIN);
        nrf_gpio_pin_set(GPIO_GPS_PWR_PIN);
        nrf_gpio_pin_set(GPIO_LORA_PWR_PIN);
                
        nrf_gpio_pin_set(GPIO_GPS_RESET);

        app_timer_start(setting_init_timer_id,SETTING_INIT_TIMER_INTERVAL,NULL);
        app_timer_start(rtc_timer_id,RTC_TIMER_INTERVAL,NULL);
        //app_timer_start(accel_timer_id,ACCEL_TIMER_INTERVAL,NULL);
            
        power_flag = 1;
        
        clear_led();
        led_state = 1;
        
        if(1 == network_mode[1]){
            advertising_flag = 1;
            advertising_init();
            debug_print(0,"Beacon Start\n");

            advertising_start();
        }
    }
    // USB가 꽂히면 바로 켜지도록
    else if(1 == nrf_gpio_pin_read(GPIO_USB_PIN)){
        debug_print(0,"USB In\n");
        
        nrf_gpio_pin_set(GPIO_POWER_ON_PIN);
        nrf_gpio_pin_set(GPIO_GPS_V_PWR_PIN);
        nrf_gpio_pin_set(GPIO_GPS_PWR_PIN);
        nrf_gpio_pin_set(GPIO_LORA_PWR_PIN);
        
        nrf_gpio_pin_set(GPIO_GPS_RESET);
        app_timer_start(setting_init_timer_id,SETTING_INIT_TIMER_INTERVAL,NULL);
        app_timer_start(rtc_timer_id,RTC_TIMER_INTERVAL,NULL);
        //app_timer_start(accel_timer_id,ACCEL_TIMER_INTERVAL,NULL);
            
        power_flag = 1;
        
        clear_led();
        led_state = 1;
        
        if(1 == network_mode[1]){
            advertising_flag = 1;
            advertising_init();
            debug_print(0,"Beacon Start\n");

            advertising_start();
        }
    }
    
    app_timer_start(power_timer_id,POWER_TIMER_INTERVAL,NULL);
	
    // Enter main loop.
    for (;;)
    {
        power_manage();
    }
}

/** 
 * @}
 */
