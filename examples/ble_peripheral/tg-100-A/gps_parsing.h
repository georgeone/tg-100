
#ifndef GPS_PARSING_H__
#define GPS_PARSING_H__

#include <stdint.h>

#define GPGGA_LATITUDE										3
#define GPGGA_LONGITUDE										5
#define GPGGA_NUMBER_SATELLITES						8														
#define GPGGA_ALTITUDE 										10

#define GPGSA_FIX_STATUS									3

#define GPGSV_FIRST_SATELLITE							5
#define GPGSV_FIRST_SNR										8
#define GPGSV_SECOND_SATELLITE						9
#define GPGSV_SECOND_SNR									12
#define GPGSV_THIRD_SATELLITE							13
#define GPGSV_THIRD_SNR										16
#define GPGSV_FOURTH_SATELLITE						17
#define GPGSV_FOURTH_SNR									20

#define GPRMC_UTCTIME											2
#define GPRMC_DATA_VALID									3
#define GPRMC_LATITUDE										4
#define GPRMC_NS													5
#define GPRMC_LONGITUDE										6
#define GPRMC_EW													7
#define GPRMC_SPEED												8
#define GPRMC_DATE												10
#define GPRMC_POSITION_MODE								11

void gps_parsing(char *receive_data,uint8_t p_index);
void packet_initialize(void);
uint16_t little_to_big16(uint16_t target);
uint32_t little_to_big32(uint32_t target);

void test_gps_parsing(char *receive_data,uint8_t p_index);
// RTC data
typedef struct{
	uint8_t hour;
	uint8_t minute;
	uint8_t second;
	uint8_t day;
} time_t;


#endif  /* _ GPS_PARSING_H__*/
