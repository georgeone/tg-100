
#include <stdint.h>
#include <string.h>
#include "timestamp.h"

uint32_t date_time_to_epoch(date_time_t* date_time)
{
    uint32_t second = date_time->second;  // 0-59
    uint32_t minute = date_time->minute;  // 0-59
    uint32_t hour   = date_time->hour;    // 0-23
    uint32_t day    = date_time->day-1;   // 0-30
    uint32_t month  = date_time->month-1; // 0-11
    uint32_t year   = date_time->year+30;    // 0-99
	
    return (((year/4*(365*4+1)+days[year%4][month]+day)*24+hour)*60+minute)*60+second;
}


void epoch_to_date_time(date_time_t* date_time,uint32_t epoch)
{
    date_time->second = epoch%60; epoch /= 60;
    date_time->minute = epoch%60; epoch /= 60;
    date_time->hour   = epoch%24; epoch /= 24;

    uint32_t years = epoch/(365*4+1)*4; epoch %= 365*4+1;

    uint32_t year;
    for (year=3; year>0; year--)
    {
        if (epoch >= days[year][0])
            break;
    }

    uint32_t month;
    for (month=11; month>0; month--)
    {
        if (epoch >= days[year][month])
            break;
    }

    date_time->year  = years+year;
    date_time->month = month+1;
    date_time->day   = epoch-days[year][month]+1;
}
