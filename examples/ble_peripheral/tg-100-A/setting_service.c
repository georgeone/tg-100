
#include "common.h"
#include "setting_service.h"
#include "app_error.h"

/**@brief     Function for handling the @ref BLE_GAP_EVT_CONNECTED event from the S110 SoftDevice.
 *
 * @param[in] p_setting     setting Service structure.
 * @param[in] p_ble_evt Pointer to the event received from BLE stack.
 */
static void on_connect(ble_setting_t * p_setting, ble_evt_t * p_ble_evt){
    p_setting->conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
}

/**@brief     Function for handling the @ref BLE_GAP_EVT_DISCONNECTED event from the S110
 *            SoftDevice.
 *
 * @param[in] p_setting     setting Service structure.
 * @param[in] p_ble_evt Pointer to the event received from BLE stack.
 */
static void on_disconnect(ble_setting_t * p_setting, ble_evt_t * p_ble_evt){
    UNUSED_PARAMETER(p_ble_evt);
    p_setting->conn_handle = BLE_CONN_HANDLE_INVALID;
}


/**@brief     Function for handling the @ref BLE_GATTS_EVT_WRITE event from the S110 SoftDevice.
 *
 * @param[in] p_setting     setting Service structure.
 * @param[in] p_ble_evt Pointer to the event received from BLE stack.
 */
static void on_write(ble_setting_t * p_setting, ble_evt_t * p_ble_evt)
{
    ble_gatts_evt_write_t * p_evt_write = &p_ble_evt->evt.gatts_evt.params.write;

    if(p_evt_write->handle == p_setting->noti_handle.cccd_handle){
        if (ble_srv_is_notification_enabled(p_evt_write->data)){
            p_setting->is_notification_enabled = true;
        }
        else{
            p_setting->is_notification_enabled = false;
        }
    }

    else if ( (p_evt_write->handle == p_setting->write_handle.value_handle)  &&  (p_setting->data_handler != NULL)  ) {
        p_setting->data_handler(p_setting, p_evt_write->data, p_evt_write->len);
    }

    else{
        // Do Nothing. This event is not relevant to this service.
    }
}


void ble_setting_on_ble_evt(ble_setting_t * p_setting, ble_evt_t * p_ble_evt)
{
    if ((p_setting == NULL) || (p_ble_evt == NULL)) {
        return;
    }
        
    switch (p_ble_evt->header.evt_id) {        
        case BLE_GAP_EVT_CONNECTED:
            on_connect(p_setting, p_ble_evt);
            break;

        case BLE_GAP_EVT_DISCONNECTED:
            on_disconnect(p_setting, p_ble_evt);
            break;

        case BLE_GATTS_EVT_WRITE:
            on_write(p_setting, p_ble_evt);
            break;

        default:
            // No implementation needed.
            break;
    }
}

static uint32_t write_char_add(ble_setting_t *p_setting){
    uint32_t err_code;
    ble_uuid_t	char_uuid;
    ble_uuid128_t base_uuid = BLE_SETTING_UUID;
    char_uuid.uuid = BLE_SETTING_WRITE_CHARACTERISTIC;

    err_code = sd_ble_uuid_vs_add(&base_uuid, &char_uuid.type);
    APP_ERROR_CHECK(err_code);

    ble_gatts_char_md_t  char_md;
    memset(&char_md, 0, sizeof(char_md));
    char_md.char_props.write = 1;

    ble_gatts_attr_md_t attr_md;
    memset(&attr_md, 0, sizeof(attr_md));  
    attr_md.vloc = BLE_GATTS_VLOC_USER;
    attr_md.vlen = 1;

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);

    ble_gatts_attr_t    attr_char_value;
    memset(&attr_char_value, 0, sizeof(attr_char_value));

    attr_char_value.p_uuid = &char_uuid;
    attr_char_value.p_attr_md = &attr_md;
    attr_char_value.max_len = QUEUED_WRITE_BUFFER_SIZE;

    err_code = sd_ble_gatts_characteristic_add(p_setting->service_handle, & char_md, &attr_char_value, &p_setting->write_handle);
    APP_ERROR_CHECK(err_code);

    return err_code;
}

static uint32_t noti_char_add(ble_setting_t *p_setting){
    uint32_t            err_code;
    ble_uuid_t          char_uuid;
    ble_uuid128_t       base_uuid = BLE_SETTING_UUID;
    char_uuid.uuid      = BLE_SETTING_NOTI_CHARACTERISTIC;

    err_code = sd_ble_uuid_vs_add(&base_uuid, &char_uuid.type);
    APP_ERROR_CHECK(err_code);

    ble_gatts_char_md_t char_md;
    memset(&char_md, 0, sizeof(char_md));

    ble_gatts_attr_md_t cccd_md;
    memset(&cccd_md, 0, sizeof(cccd_md));
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.write_perm);
    cccd_md.vloc                = BLE_GATTS_VLOC_STACK;    
    char_md.char_props.notify   = 1;  
    char_md.p_char_user_desc    = NULL;
    char_md.p_char_pf           = NULL;
    char_md.p_user_desc_md      = NULL;
    char_md.p_cccd_md           = &cccd_md;
    char_md.p_sccd_md           = NULL;

    ble_gatts_attr_md_t attr_md;
    memset(&attr_md, 0, sizeof(attr_md));  
    attr_md.vloc    = BLE_GATTS_VLOC_STACK;    
    attr_md.rd_auth = 0;
    attr_md.wr_auth = 0;
    attr_md.vlen    = 1;

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);

    ble_gatts_attr_t    attr_char_value;
    memset(&attr_char_value, 0, sizeof(attr_char_value)); 
    attr_char_value.p_uuid    = &char_uuid;
    attr_char_value.p_attr_md = &attr_md;   
    attr_char_value.init_len  = 0;
    attr_char_value.init_offs = 0;
    attr_char_value.max_len   = 20;

    err_code = sd_ble_gatts_characteristic_add(p_setting->service_handle, &char_md, &attr_char_value, &p_setting->noti_handle);
    APP_ERROR_CHECK(err_code);

    return err_code;
}

uint32_t ble_setting_init(ble_setting_t * p_setting, const ble_setting_init_t * p_setting_init){
    uint32_t err_code;
    ble_uuid_t service_uuid;
    ble_uuid128_t base_uuid = BLE_SETTING_UUID;
    service_uuid.uuid = BLE_SETTING_SERVICE;

    p_setting->data_handler = p_setting_init->data_handler;

    err_code =  sd_ble_uuid_vs_add(&base_uuid, &service_uuid.type);
    APP_ERROR_CHECK(err_code);

    err_code = sd_ble_gatts_service_add(BLE_GATTS_SRVC_TYPE_PRIMARY, &service_uuid, &p_setting->service_handle);
    APP_ERROR_CHECK(err_code);

    write_char_add(p_setting);
    noti_char_add(p_setting);

    return err_code;
}

uint32_t setting_notification_send(ble_setting_t *p_setting, uint8_t * p_string, uint16_t length){
    ble_gatts_hvx_params_t hvx_params;

    if (p_setting->conn_handle == BLE_CONN_HANDLE_INVALID || (!p_setting->is_notification_enabled)){
        return NRF_ERROR_INVALID_STATE;
    }

    if (length > 20){
        return NRF_ERROR_INVALID_PARAM;
    }

    memset(&hvx_params, 0, sizeof(hvx_params));

    hvx_params.handle = p_setting->noti_handle.value_handle;
    hvx_params.p_data = p_string;
    hvx_params.p_len  = &length;
    hvx_params.type   = BLE_GATT_HVX_NOTIFICATION;

    return sd_ble_gatts_hvx(p_setting->conn_handle, &hvx_params);
	
}
