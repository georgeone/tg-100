
#include <string.h>
#include <stdint.h>
#include "nrf.h"
#include "batt.h"
#include "led_operation.h"

extern uint8_t led_state;

uint8_t batt_adc_check(){

	uint16_t battery;
	
  // interrupt ADC
	NRF_ADC->INTENSET = (ADC_INTENSET_END_Disabled << ADC_INTENSET_END_Pos);						//!< Interrupt enabled. 
    
	// config ADC
		NRF_ADC->CONFIG = (ADC_CONFIG_EXTREFSEL_None << ADC_CONFIG_EXTREFSEL_Pos) // Bits 17..16 : ADC external reference pin selection. 
		| (ADC_CONFIG_PSEL_AnalogInput2 << ADC_CONFIG_PSEL_Pos)				//	!< Use analog input 6 as analog input. 
		| (ADC_CONFIG_REFSEL_VBG << ADC_CONFIG_REFSEL_Pos)						//	!< Use internal 1.2V bandgap voltage as reference for conversion. 
		| (ADC_CONFIG_INPSEL_AnalogInputOneThirdPrescaling << ADC_CONFIG_INPSEL_Pos) ///*!< Analog input specified by PSEL with 1/3 prescaling used as input for the conversion. 
		| (ADC_CONFIG_RES_8bit << ADC_CONFIG_RES_Pos);					//				!< 8bit ADC resolution. 
  
    
	// enable ADC		
	NRF_ADC->ENABLE = ADC_ENABLE_ENABLE_Enabled;					  													// Bit 0 : ADC enable. 	
	

	NRF_ADC->TASKS_START = 1;							//Start ADC sampling

	// wait for conversion to end
	while (!NRF_ADC->EVENTS_END)
	{}
		
	//end 		
	NRF_ADC->EVENTS_END = 0;			
		
	//Save your ADC result
	battery = NRF_ADC->RESULT;	
	
	//Use the STOP task to save current. Workaround for PAN_028 rev1.1 anomaly 1.
	NRF_ADC->TASKS_STOP = 1;

	// disable ADC		
	NRF_ADC->ENABLE = ADC_ENABLE_ENABLE_Disabled;
		
	// 충전 중일 때 전압은 0.1v 높게 측정되므로 0.1v 다운시켜 battery 표시	
	// cut off 기준 3.3v
	if(1 == nrf_gpio_pin_read(GPIO_USB_PIN)){
		
		// charge_pin에 HIGH가 들어오면 충전 완료
		if(1 == nrf_gpio_pin_read(GPIO_CHARGE_PIN)){
			battery = 100;
		}
		// charge_pin에 LOW가 들어올 경우
		else{
			if(battery >= 180){
				battery = 90;
			}
			else if(battery < 180 && battery >= 178){
				battery = 80;
			}
			else if(battery < 178 && battery >= 176){
				battery = 70;
			}
			else if(battery < 176 && battery >= 173){
				battery = 60;
			}
			else if(battery < 173 && battery >= 171){
				battery = 50;
			}
			else if(battery < 171 && battery >= 169){
				battery = 40;
			}
			else if(battery < 169 && battery >= 167){
				battery = 30;
			}
			else if(battery < 167 && battery >= 164){
				battery = 20;
			}
			else if(battery < 164 && battery >= 162){
				battery = 10;
			}
			else if(battery < 162){
				battery = 1;
			}
		}
	}
	// 충전하지 않을 때 전압 측정하여 battery 표시
	else{
		if(battery >= 183){
			battery = 100;
		}
		else if(battery < 183 && battery >= 176){
			battery = 90;
		}
		else if(battery < 176 && battery >= 173){
			battery = 80;
		}
		else if(battery < 173 && battery >= 171){
			battery = 70;
		}
		else if(battery < 171 && battery >= 169){
			battery = 60;
		}
		else if(battery < 169 && battery >= 167){
			battery = 50;
		}
		else if(battery < 167 && battery >= 164){
			battery = 40;
		}
		else if(battery < 164 && battery >= 162){
			battery = 30;
		}
		else if(battery < 162 && battery >= 160){
			battery = 20;
		}
		else if(battery < 160 && battery >= 152){
			battery = 10;
		}
		else if(battery < 152 && battery >= 146){
			battery = 1;
		}
		else if(battery < 146){
			battery = 0;
		}
	}
	
	return battery;
}
