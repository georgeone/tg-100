
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "led_operation.h"
#include "gps_parsing.h"
#include "lora_parsing.h"
#include "timestamp.h"
#include "SEGGER_RTT.h"

// Extern Variable
extern uint8_t led_state;
extern time_t time;
extern uint8_t 	packet_header[8];																																				
extern uint8_t 	packet_version; 																																				
extern uint8_t 	major_minor[8];																																					
extern uint8_t	lora_eui[8];																																						
extern uint8_t 	error_range[4];																																					
extern uint8_t 	firmware_version[3];																																		
extern uint8_t 	packet[46];																																							
extern uint8_t	m_beacon_info[23];
extern uint8_t	battery_state;
extern uint32_t	gps_gathering_time;
extern uint8_t	network_mode[4];
extern uint8_t	max_xyz_noti[3];
extern uint32_t	timestamp;

// Flag
extern uint8_t	function_key_flag;
extern uint8_t	gyro_flag;
extern uint8_t	gps_flag;
extern uint8_t 	gps_valid_flag;
extern uint8_t 	no_fix_gps_uart_stop_flag;
extern uint8_t	fix_gps_uart_stop_flag;
extern uint8_t 	fixed_uuid_flag;
extern uint8_t	gps_no_data_flag;
extern uint8_t	satellite_flag;
extern uint8_t	test_result_flag;
static uint8_t 	data_valid_flag;
static uint8_t	data_fix_flag;

// Static Variable
static uint8_t satellite;
static int16_t altitude;
static uint32_t latitude;
static uint32_t longitude;
static uint16_t speed;
static uint8_t status;

static uint8_t test_latitude;
static uint8_t test_longitude;

// Extern function
extern void advertising_init(void);
extern void uart_stop(void);

uint32_t little_to_big32(uint32_t target){
	return ((target&0xff000000)>>24)| ((target&0xff0000)>>8) | ((target&0xff00)<<8) | ((target&0xff)<<24);
}

uint16_t little_to_big16(uint16_t target){
	return (target>>8) | (target<<8);
}

// Get day
uint8_t day_from_date(int y, int m, int d)  
{  
   /*
   int c;
	 y += 2000;
   if (m < 3) {  
     y--;  
     m += 10;  
   }  
   else {  
     m -= 2;  
   }  
   c = y / 100;  
   y = y % 100;  
  
   return (d + (26*m-2)/10 - 2*c + y + y/4 + c/4) % 7;  
    */
    static int t[] = { 0, 3, 2, 5, 0, 3, 5, 1, 4, 6, 2, 4 };
    y+=2000;
	y -= m < 3;

	return(y + y / 4 - y / 100 + y / 400 + t[m - 1] + d) % 7;
}  

void packet_initialize(void){
	
		timestamp = little_to_big32(timestamp);
	
		memcpy(&packet[0],&packet_header[1],5);
		memcpy(&packet[5],&packet_version,1);
		memcpy(&packet[6],&major_minor[1],4);
		memcpy(&packet[10],&lora_eui[4],4);
		memcpy(&packet[14],&timestamp,4);
		memcpy(&packet[34],&error_range,4);
		memcpy(&packet[38],&battery_state,1);
		memcpy(&packet[40],&firmware_version,3);
		memcpy(&packet[43],&max_xyz_noti,3);
	
		memset(max_xyz_noti,0,sizeof(max_xyz_noti));
		timestamp = little_to_big32(timestamp);
	
		packet[39] &= ~(0x03 << 0);
	
		if( 1 == function_key_flag ){
			packet[39] &= ~(0x01 << 2);
			packet[39] |= (0x01 << 2);
		}
		
		if( 1 == gyro_flag ){
			packet[39] |= (0x01 << 0);
			if( 1 == gps_flag ){
				packet[39] |= (0x01 << 1);
				gyro_flag = 0;
				gps_flag = 0;
			}
		}
		
}

void gps_parsing(char *receive_data,uint8_t p_index){
	#if 0
	if(strncmp(receive_data,"$PMTK001,314,3*36",strlen("$PMTK001,314,3*36")) == 0 && gps_no_data_flag == 1){
		gps_no_data_flag = 0;
	}
	else
    #endif
    {
		
		char *token = NULL;
		int data_pos = 0;
		token = strtok(receive_data,",*");
											
		if(strncmp(token,"$GPGGA",strlen("$GPGGA")) == 0){
            while(token != NULL){
													
				data_pos++;
							
				if(data_pos == GPGGA_NUMBER_SATELLITES){
					satellite = atoi(token);
					if(1 == network_mode[1] && 0 == fixed_uuid_flag){
						memcpy(&m_beacon_info[16],&satellite,1);
					}
					if(6 <= satellite){
						satellite_flag = 1;
                        SEGGER_RTT_printf(0,"////////////////////////////////satellite_flag//////////////////////////////////////////////\n");
					}
				}										
				else if(data_pos == GPGGA_ALTITUDE){ 
					altitude = (int16_t)(atof(token)*10);		
					altitude = little_to_big16(altitude);
				}
													
				token = strtok(NULL,",*");
			}
			
		}
		
		else if(strncmp(token,"$GNGSA",strlen("$GNGSA")) == 0){
            while(token != NULL){
				
				data_pos++;
				
				if(data_pos == GPGSA_FIX_STATUS){
					if(strcmp(token,"3")==0){
						data_fix_flag = 1;
					}
				}
				
				token = strtok(NULL,",*");
			}
			
		}
		
		else if(strncmp(token,"$GNRMC",strlen("$GNRMC")) == 0){
            float f_latitude;
			float f_longitude;
			date_time_t date_time;
												
			while(token != NULL){
													
				data_pos++;
													
				if(data_pos == GPRMC_UTCTIME){
					char s_time[3];
					memset(s_time,0,sizeof(s_time));
					int gps_time;
					for(int i=0;i<=4;i=i+2){
						strncpy(s_time,&token[i],2);
						gps_time = atoi(s_time);
						if(0 == i){
							date_time.hour = gps_time;
						}
						else if(2 == i){
							date_time.minute = gps_time;
						}
						else if(4 == i){
							date_time.second = gps_time;
						}
					}
				}
													
				else if(data_pos == GPRMC_DATA_VALID){
					if(strcmp(token,"A")==0){
						data_valid_flag = 1;
					}
					else{
						data_valid_flag = 0;
					}
					if(1 == network_mode[1] && 0 == fixed_uuid_flag)
						memcpy(&m_beacon_info[17],&data_valid_flag,1);
				}
													
				else if(data_pos == GPRMC_LATITUDE){
					char s_gps_d[3];
					memset(s_gps_d,0,sizeof(s_gps_d));
					int gps_d;
					float gps_m;
					strncpy(s_gps_d,token,2);
					gps_d = atoi(s_gps_d);
					gps_m = strtof(&token[2],NULL)/60 + gps_d;
					f_latitude = gps_m;
				}
													
				else if(data_pos == GPRMC_NS){
					if(0 == strcmp(token,"S")){
						f_latitude *= -1;
					}
					memcpy(&latitude,&f_latitude,4);
					latitude = little_to_big32(latitude);
					if(1 == network_mode[1] && 0 == fixed_uuid_flag)
						memcpy(&m_beacon_info[5],&latitude,4);
				}
													
				else if(data_pos == GPRMC_LONGITUDE){
					char s_gps_d[4];
					memset(s_gps_d,0,sizeof(s_gps_d));
					int gps_d;
					float gps_m;
					strncpy(s_gps_d,token,3);
					gps_d = atoi(s_gps_d);
					gps_m = strtof(&token[3],NULL)/60 + gps_d;
					f_longitude = gps_m;
				}
													
				else if(data_pos == GPRMC_EW){
					if(0 == strcmp(token,"W")){
					f_longitude *= -1;
					}
					memcpy(&longitude,&f_longitude,4);
					longitude = little_to_big32(longitude);
					if(1 == network_mode[1] && 0 == fixed_uuid_flag){
						memcpy(&m_beacon_info[9],&longitude,4);
						advertising_init();
					}
				}
													
				else if(data_pos == GPRMC_SPEED){
					speed = (uint16_t)(atof(token) * 1.852 * 10);
					speed = little_to_big16(speed);
				}
													
				else if(data_pos == GPRMC_DATE){
					char s_date[3];
					memset(s_date,0,sizeof(s_date));
					int date;
					for(int i=0;i<=4;i=i+2){
						strncpy(s_date,&token[i],2);
						date = atoi(s_date);
						if(0 == i)
							date_time.day = date;
						else if(2 == i)
							date_time.month = date;
						else if(4 == i)
							date_time.year = date;
						}
				}
													
				else if(data_pos == GPRMC_POSITION_MODE){
					if(0 == strcmp(token,"A")){
						status = 1;
					}
					else if(0 == strcmp(token,"D")){
						status = 2;
					}
					else if(0 == strcmp(token,"N")){
						status = 0;
					}
				}
													
					token = strtok(NULL,",*");
			}
									
			
			if(1 == data_valid_flag && 1 == data_fix_flag){
				
				if(1 == gps_valid_flag){
					fix_gps_uart_stop_flag = 1;
                    SEGGER_RTT_printf(0,"////////////////////////////////gps_valid_flag//////////////////////////////////////////////\n");
				}
				
				gps_valid_flag = 1;
				gps_gathering_time = 0;
				
				data_valid_flag = 0;
				data_fix_flag = 0;
				
				time.hour = date_time.hour;
				time.minute = date_time.minute;
				time.second = date_time.second;
				time.day = day_from_date(date_time.year,date_time.month,date_time.day);
				
				timestamp = date_time_to_epoch(&date_time);
				
				memcpy(&packet[33],&satellite,1);
				memcpy(&packet[26],&altitude,2);
				memcpy(&packet[18],&latitude,4);
				memcpy(&packet[22],&longitude,4);
				memcpy(&packet[28],&speed,2);
				if(0 == status){
					packet[39] &= ~(0x03 << 3);
				}
				else if(1 == status){
					packet[39] &= ~(0x01 << 3);
					packet[39] |= (0x01 << 3);
				}
				else if(2 == status){
					packet[39] &= ~(0x01 << 4);
					packet[39] |= (0x01 << 4);
				}
			
			}
				
			else{
				
				if(0 == gps_valid_flag){
					clear_led();
					led_state = 7;
				}
			
			}
			
			no_fix_gps_uart_stop_flag = 1;
		}
	}
}

void test_gps_parsing(char *receive_data,uint8_t p_index){
	
	int data_pos = 0;
	char *token = NULL;
	token = strtok(receive_data,",*");
	
	if(strncmp(token,"$GPGGA",strlen("$GPGGA")) == 0){
		
		while(token != NULL){
			
			data_pos++;
			
			if(data_pos == GPGGA_LATITUDE){
				char s_gps_d[3];
				memset(s_gps_d,0,sizeof(s_gps_d));
				strncpy(s_gps_d,token,2);
				test_latitude = atoi(s_gps_d);
			}
			else if(data_pos == GPGGA_LONGITUDE){
				char s_gps_d[4];
				memset(s_gps_d,0,sizeof(s_gps_d));
				strncpy(s_gps_d,token,3);
				test_longitude = atoi(s_gps_d);
			}
			else if(data_pos == GPGGA_NUMBER_SATELLITES){
				satellite = atoi(token);
			}
			token = strtok(NULL,",*");
		}
			
	}
	
	else if(strncmp(token,"$GNGSA",strlen("$GNGSA")) == 0){
		
		while(token != NULL){
			
			data_pos++;
			
			if(data_pos == GPGSA_FIX_STATUS){
				if(strcmp(token,"3")==0){
					data_fix_flag = 1;
				}
			}
			token = strtok(NULL,",*");
		}
		
		if(1 == data_fix_flag){
			
			data_fix_flag = 0;
			
			test_result_flag = 1;
			
			m_beacon_info[7] = test_latitude;
			m_beacon_info[8] = test_longitude;
			m_beacon_info[9] = satellite;
		}
		
		m_beacon_info[5] = 1;
		advertising_init();
	}
}
