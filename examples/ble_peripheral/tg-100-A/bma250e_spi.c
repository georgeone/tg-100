#include <stdbool.h>
#include "bma250e_spi.h"
#include "nrf_drv_spi.h"
#include "boards.h"
#include "app_error.h"
#include "app_util_platform.h"
#include "nrf_gpio.h"
#include <string.h>

static const nrf_drv_spi_t *bma250e_spi;

void bma250e_spi_init(const nrf_drv_spi_t *m_spi_master){
	
	bma250e_spi = m_spi_master;
	
}

void bma250e_spi_readRegister(uint8_t address, uint8_t * p_data, uint8_t bytes){
    uint32_t err_code;
        
    uint8_t m_tx_data_spi;
        
    m_tx_data_spi = 0x80|address;
    
    err_code = nrf_drv_spi_transfer(bma250e_spi, &m_tx_data_spi, bytes, p_data, bytes);
    APP_ERROR_CHECK(err_code);

}

void bma250e_spi_writeRegister(uint8_t *p_data, uint8_t bytes){
         
    uint32_t err_code;
	
    err_code = nrf_drv_spi_transfer(bma250e_spi, p_data, bytes, NULL, 0);
    APP_ERROR_CHECK(err_code);
    
}

void bma250e_spi_readAccelerometerData( uint8_t * rx_buffer, uint8_t rx_buf_size, int16_t *x_val, int16_t *y_val, int16_t *z_val,int8_t *xyz_noti){  
    
    bma250e_spi_readRegister(0x02,rx_buffer,rx_buf_size);
  
    *x_val = (rx_buffer[2]<<8) | rx_buffer[1];
		*x_val >>= 6;
		xyz_noti[0] = rx_buffer[2];
    *y_val = (rx_buffer[4]<<8) | rx_buffer[3];
		*y_val >>= 6;
		xyz_noti[1] = rx_buffer[4];
    *z_val = (rx_buffer[6]<<8) | rx_buffer[5];
		*z_val >>= 6;
		xyz_noti[2] = rx_buffer[6];
       
}

