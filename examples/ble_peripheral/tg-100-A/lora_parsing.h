
#ifndef LORA_PARSING_H__
#define LORA_PARSING_H__

#include <stdint.h>

#define DEFAULT_DATA_BLOCK	0

void lora_parsing(char *receive_data,uint8_t p_index);
void command_enqueue(void);
void command_dequeue(void);
uint8_t app_key_check(uint8_t *app_key);
uint8_t eui_check(uint8_t *eui);

void test_lora_parsing(char *receive_data,uint8_t p_index);

#endif  /* _ LORA_PARSING_H__*/
