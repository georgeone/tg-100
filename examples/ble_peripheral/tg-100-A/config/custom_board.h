
#define GPIO_BATT_DET						1

#define GPIO_LORA_RESET_PIN					0
#define GPIO_LORA_TX_PIN 					2
#define GPIO_LORA_RX_PIN					3
#define GPIO_LORA_DEBUG_RX_PIN				4
#define GPIO_LORA_DEBUG_TX_PIN				5
#define GPIO_LORA_PWR_PIN					6
#define GPIO_LORA_INTP_PIN					7

#define GPIO_SCK_PIN       					12   
#define GPIO_MOSI_PIN     					11    
#define GPIO_MISO_PIN     					10     
#define GPIO_SS_PIN        					13  

#define GPIO_AC_INTP1_PIN					14
#define GPIO_AC_INTP2_PIN					15

#define GPIO_STATUS_RED_LED					16
#define GPIO_GPS_RX_PIN						17
#define GPIO_GPS_TX_PIN						18
#define GPIO_STATUS_BLUE_LED				19

#define GPIO_GPS_V_PWR_PIN					20

#define GPIO_CHARGE_PIN						21
#define GPIO_USB_PIN						22

#define GPIO_KEY_PIN						23

#define GPIO_POWER_BUTTON_PIN				24
#define GPIO_POWER_ON_PIN					25

#define GPIO_GPS_PWR_PIN					29
#define GPIO_GPS_ON_OFF						26
#define GPIO_GPS_SYSTEM_ON					27
#define GPIO_GPS_RESET				    	28

#define NRF_CLOCK_LFCLKSRC      {.source        = NRF_CLOCK_LF_SRC_RC,            \
                                 .rc_ctiv       = 16,                                \
                                 .rc_temp_ctiv  = 0,                                \
                                 .xtal_accuracy = NRF_CLOCK_LF_XTAL_ACCURACY_20_PPM}
