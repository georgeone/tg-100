
#ifndef LED_OPERATION_H__
#define LED_OPERATION_H__

#include <stdint.h>
#include "nrf_soc.h"
#include "nrf_gpio.h"
#include "custom_board.h"

void clear_led(void);
void led_operation(void);

#endif  /* _ LED_OPERATION_H__*/
