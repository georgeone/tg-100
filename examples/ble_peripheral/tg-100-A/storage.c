
#include "storage.h"
#include "pstorage.h"
#include "app_error.h"

static pstorage_handle_t p_id;   
static pstorage_handle_t p_block_id;  

static int  pstorage_wait_flag = 0;
//static pstorage_block_t pstorage_wait_handle = 0;

extern void power_manage(void);

//wait p_storage operation
void wait_till_op(){
    
	while (pstorage_wait_flag)	{	
        power_manage();	
	}
	
}

static void example_cb_handler(pstorage_handle_t  * handle, uint8_t  op_code, 
																									uint32_t result, uint8_t * p_data, uint32_t data_len){
																										
  //if(handle->block_id == pstorage_wait_handle) { pstorage_wait_flag = 0; }
	pstorage_wait_flag = 0;
																										
	switch (op_code)	{
        case PSTORAGE_STORE_OP_CODE:
            break;

        case PSTORAGE_LOAD_OP_CODE:
            break;

        case PSTORAGE_CLEAR_OP_CODE:
            break;

        case PSTORAGE_UPDATE_OP_CODE:
            break;
        //error 
        default:   
            break;
	}																										
}

void storage_init(int block_size, int block_count){

	uint32_t err_code;
	pstorage_module_param_t p_param;
 
	p_param.block_size = block_size;   
	p_param.block_count = block_count;   
	p_param.cb = example_cb_handler;

	//err_code = pstorage_init();
	//APP_ERROR_CHECK(err_code);

	err_code = pstorage_register(&p_param, &p_id);    
	APP_ERROR_CHECK(err_code);    
}

void storage_load(int ident, uint8_t * dest, int size, int offset){
	
	uint32_t err_code;
	err_code = pstorage_block_identifier_get(&p_id, ident, &p_block_id);    
	APP_ERROR_CHECK(err_code);

	//pstorage_wait_handle = p_block_id.block_id;
	pstorage_wait_flag = 1;
	
	err_code = pstorage_load(dest, &p_block_id, size, offset);		
	APP_ERROR_CHECK(err_code);
	
	wait_till_op();
}

void storage_save(int ident, uint8_t * source, int size, int offset){
	uint32_t err_code;
	err_code = pstorage_block_identifier_get(&p_id, ident, &p_block_id);
	APP_ERROR_CHECK(err_code);
  
	//pstorage_wait_handle = p_block_id.block_id;
	//pstorage_wait_flag = 1;
  
	err_code = pstorage_store(&p_block_id, source, size, offset); 
  APP_ERROR_CHECK(err_code);
	
	wait_till_op();
}

void storage_clear(int block_size, int block_count){
	
	uint32_t err_code;
	err_code = pstorage_clear(&p_id, (block_size*block_count));	
	APP_ERROR_CHECK(err_code);
	
	//pstorage_wait_handle = p_block_id.block_id;
	//pstorage_wait_flag = 1;
	
	wait_till_op();
}


void storage_update(uint16_t ident, uint8_t * source, int size, int offset){
	
	uint32_t err_code;
	err_code = pstorage_block_identifier_get(&p_id, ident, &p_block_id);
	APP_ERROR_CHECK(err_code);	
	
	//pstorage_wait_handle = p_block_id.block_id;
	//pstorage_wait_flag = 1;

	err_code = pstorage_update(&p_block_id, source, size, offset);  				
	APP_ERROR_CHECK(err_code);
	
	wait_till_op();
}



