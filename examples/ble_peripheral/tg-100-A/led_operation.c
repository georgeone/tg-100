
#include <stdint.h>
#include <string.h>
#include "led_operation.h"

extern uint8_t led_state;
extern uint8_t power_flag;
extern uint8_t power_off_flag;
extern uint8_t battery_state;
extern uint8_t factory_reset_flag;

static uint8_t power_on_led_cnt				=	0;
static uint8_t power_off_led_cnt 	 		= 0;
static uint8_t charging_led_cnt 			=	0;
static uint8_t beacon_on_led_cnt			=	0;
static uint8_t beacon_off_led_cnt			=	0;
static uint16_t function_led_cnt			=	0;
static uint8_t power_on_check_led_cnt	= 0;
static uint8_t low_battery_led_cnt		=	0;
static uint8_t gps_fail_led_cnt				=	0;
static uint8_t lora_fail_led_cnt			=	0;
static uint8_t factory_reset_led_cnt	=	0;
static uint8_t test_power_on_led_cnt  = 0;
static uint8_t test_progress_led_cnt	=	0;
static uint8_t provision_fail_led_cnt = 0;

void led_control(uint8_t red, uint8_t blue){
	if(1 == red){
		nrf_gpio_pin_clear(GPIO_STATUS_RED_LED);
	}
	else{
		nrf_gpio_pin_set(GPIO_STATUS_RED_LED);
	}
	if(1 == blue){
		nrf_gpio_pin_clear(GPIO_STATUS_BLUE_LED);
	}
	else{
		nrf_gpio_pin_set(GPIO_STATUS_BLUE_LED);
	}
}

void clear_led(void){
	power_on_led_cnt 			 = 0;
	charging_led_cnt 			 = 0;
	beacon_on_led_cnt 		 = 0;
	beacon_off_led_cnt 		 = 0;
	function_led_cnt			 = 0;
	power_on_check_led_cnt = 0;
	low_battery_led_cnt		 = 0;
	gps_fail_led_cnt			 = 0;
	lora_fail_led_cnt			 = 0;
	test_power_on_led_cnt	 = 0;
	test_progress_led_cnt	 = 0;
	provision_fail_led_cnt = 0;
	
	if(0 == power_off_flag)
		led_control(0,0);
}

void power_on_led(void){
	power_on_led_cnt++;
	if(1 == power_on_led_cnt){
		led_control(1,0);
	}
	else if(30 == power_on_led_cnt){
		led_state = 0;
		clear_led();
	}
}

void power_off_led(void){
	power_off_led_cnt++;
	if(1 == power_off_led_cnt%2){
		led_control(1,0);
	}
	else{
		led_control(1,1);
	}
	if(30 == power_off_led_cnt){
		led_state = 0;
		clear_led();
		nrf_gpio_pin_clear(GPIO_POWER_ON_PIN);
		power_flag = 0;
		power_off_flag = 0;
		NVIC_SystemReset();
	}
}

void beacon_on_led(void){
	beacon_on_led_cnt++;
	if(1 == beacon_on_led_cnt%10){
		led_control(1,0);
	}
	else if(6 == beacon_on_led_cnt%10){
		led_control(0,1);
	}
	if(50 == beacon_on_led_cnt){
		led_state = 0;
		clear_led();
	}
}

void beacon_off_led(void){
	beacon_off_led_cnt++;
	if(1 == beacon_off_led_cnt){
		led_control(1,1);
	}
	else if(50 == beacon_off_led_cnt){
		led_state = 0;
		clear_led();
	}
}

void function_led(void){
	function_led_cnt++;
	if(1 == function_led_cnt%20){
		led_control(1,0);
	}
	else if(11 == function_led_cnt%20){
		led_control(0,0);
	}
	if(300 == function_led_cnt){
		led_state = 0;
		clear_led();
	}
}

void power_on_check_led(void){
	power_on_check_led_cnt++;
	if(1 == power_on_check_led_cnt){
		led_control(1,1);
	}
	else{
		led_state = 0;
		clear_led();
	}
}

void gps_fail_led(void){
	gps_fail_led_cnt++;
	if(1 == gps_fail_led_cnt){
		led_control(0,1);
	}
	else if(50 == gps_fail_led_cnt){
		led_state = 0;
		clear_led();
	}
}

void lora_fail_led(void){
	lora_fail_led_cnt++;
	if(1 == lora_fail_led_cnt){
		led_control(1,1);
	}
	else if(50 == lora_fail_led_cnt){
		led_state = 0;
		clear_led();
	}
}

void low_battery_led(void){
	low_battery_led_cnt++;
	if(0 == low_battery_led_cnt%100){
		low_battery_led_cnt = 0;
		led_control(1,0);
	}
	else{
		led_control(0,0);
	}
	if(15 < battery_state){
		led_state = 0;
		clear_led();
	}
}

void charging_led(void){
	if(1 == nrf_gpio_pin_read(GPIO_CHARGE_PIN)){
		led_control(1,1);
	}
	else{
		charging_led_cnt++;
		
		if(10 == charging_led_cnt){
			led_control(0,1);
		}
		else if(20 == charging_led_cnt){
			charging_led_cnt = 0;
			led_control(1,1);
		}
	}
	if(0 == nrf_gpio_pin_read(GPIO_USB_PIN)){
		led_state = 0;
		clear_led();
	}
}

void factory_reset_led(void){
	factory_reset_led_cnt++;
	if(1 == factory_reset_led_cnt%2){
		led_control(1,0);
	}
	else{
		led_control(0,0);
	}
}

void test_power_on_led(void){
	test_power_on_led_cnt++;
	if(1 == test_power_on_led_cnt){
		led_control(1,1);
	}
	else if(11 == test_power_on_led_cnt){
		led_state = 0;
		clear_led();
		led_state = 16;
	}
}

void test_success_led(void){
	led_control(0,1);
}

void test_fail_led(void){
	led_control(1,0);
}

void test_lora_baudrate_led(void){
	led_control(1,1);
}

void test_progress_led(void){
	test_progress_led_cnt++;
	if(10 == test_progress_led_cnt){
		led_control(0,1);
	}
	else if(20 == test_progress_led_cnt){
		led_control(0,0);
		test_progress_led_cnt = 0;
	}
}

void provision_fail_led(void){
	provision_fail_led_cnt++;
	if(11 == provision_fail_led_cnt){
		led_control(1,0);
	}
	else if(31 == provision_fail_led_cnt){
		provision_fail_led_cnt = 0;
		led_control(0,0);
	}
}

void led_operation(void){
	
	if(1 == power_off_flag){
		led_state = 2;
	}
	else if(1 == factory_reset_flag){
		led_state = 11;
	}
	
	if(0 == led_state){
		led_control(0,0);
		if(nrf_gpio_pin_read(GPIO_USB_PIN)){
			led_state = 10;
		}
		else{
				if(15 >= battery_state){
					led_state = 9;
				}
		}
	}
	else if(1 == led_state){
		power_on_led();
	}
	else if(2 == led_state){
		power_off_led();
	}
	else if(3 == led_state){
		beacon_on_led();
	}
	else if(4 == led_state){
		beacon_off_led();
	}
	else if(5 == led_state){
		function_led();
	}
	else if(6 == led_state){
		power_on_check_led();
	}
	else if(7 == led_state){
		gps_fail_led();
	}
	else if(8 == led_state){
		lora_fail_led();
	}
	else if(9 == led_state){
		low_battery_led();
	}
	else if(10 == led_state){
		charging_led();
	}
	else if(11 == led_state){
		factory_reset_led();
	}
	else if(12 == led_state){
		test_power_on_led();
	}
	else if(13 == led_state){
		test_success_led();
	}
	else if(14 == led_state){
		test_fail_led();
	}
	else if(15 == led_state){
		test_lora_baudrate_led();
	}
	else if(16 == led_state){
		test_progress_led();
	}
	else if(17 == led_state){
		provision_fail_led();
	}
		
}
