


#ifndef BLE_LORA_H__
#define BLE_LORA_H__

#include <stdint.h>
#include <stdbool.h>
#include "ble.h"
#include "ble_srv_common.h"
#include "nordic_common.h"
#include <string.h>

#define BLE_LORA_UUID                    {0x23, 0xD1, 0x13, 0xEF, 0x5F, 0x78, 0x23, 0x15, 0xDE, 0xEF, 0x12, 0x12, 0x00, 0x00, 0x00, 0x00}
#define BLE_LORA_SERVICE                0x0011 
#define BLE_LORA_WRITE_CHARACTERISTIC   0x0012                                   
#define BLE_LORA_NOTI_CHARACTERISTIC   0x0013   

#define QUEUED_WRITE_BUFFER_SIZE        350

typedef struct ble_lora_s ble_lora_t;

typedef void(*ble_lora_data_handler_t) (ble_lora_t * p_lora, uint8_t * data, uint16_t length);

typedef struct
{
	ble_lora_data_handler_t        data_handler;                       
} ble_lora_init_t;


typedef struct ble_lora_s
{
	uint16_t                     conn_handle;                          
	uint16_t                     service_handle;                     
	ble_gatts_char_handles_t	write_handle; 
	ble_gatts_char_handles_t	noti_handle;
	bool                     is_notification_enabled; 
	ble_lora_data_handler_t   data_handler;                          

} ble_lora_t;

static uint8_t queued_write_buffer[QUEUED_WRITE_BUFFER_SIZE];
static ble_user_mem_block_t mem_block;

uint32_t ble_lora_init(ble_lora_t * p_lora, const ble_lora_init_t * p_lora_init);

void ble_lora_on_ble_evt(ble_lora_t * p_lora, ble_evt_t * p_ble_evt);

void lora_data_handler(ble_lora_t * p_lora, uint8_t * p_data, uint16_t length);

uint32_t lora_notification_send(ble_lora_t * p_lora, uint8_t *p_string, uint16_t length);

#endif // BLE_LORA_H__







