#include <stdint.h>
#include "nrf_drv_spi.h"

void bma250e_spi_init(const nrf_drv_spi_t *m_spi_master);

void bma250e_spi_readRegister(uint8_t address, uint8_t * p_data, uint8_t bytes);

void bma250e_spi_writeRegister(uint8_t * p_data, uint8_t bytes);
 
void bma250e_spi_readAccelerometerData( uint8_t * rx_buffer, uint8_t rx_buf_size, int16_t *x_val, int16_t *y_val, int16_t *z_val, int8_t *xyz_noti);
