#ifndef BLE_SETTING_H__
#define BLE_SETTING_H__

#include <stdint.h>
#include <stdbool.h>
#include "ble.h"
#include "ble_srv_common.h"
#include "nordic_common.h"
#include <string.h>

#define BLE_SETTING_UUID                    {0x23, 0xD1, 0x13, 0xEF, 0x5F, 0x78, 0x23, 0x15, 0xDE, 0xEF, 0x12, 0x12, 0x00, 0x00, 0x00, 0x00}
#define BLE_SETTING_SERVICE                	0x0001 
#define BLE_SETTING_WRITE_CHARACTERISTIC   	0x0002                                   
#define BLE_SETTING_NOTI_CHARACTERISTIC		0x0003
#define QUEUED_WRITE_BUFFER_SIZE        	350

typedef struct ble_setting_s ble_setting_t;
typedef void(*ble_setting_data_handler_t) (ble_setting_t * p_setting, uint8_t * data, uint16_t length);

typedef struct
{
	ble_setting_data_handler_t        data_handler;                           
} ble_setting_init_t;

typedef struct ble_setting_s
{
	uint16_t                     conn_handle;                           
	uint16_t                     service_handle;                        
	ble_gatts_char_handles_t	 write_handle;
	ble_gatts_char_handles_t	 noti_handle;
	bool                         is_notification_enabled; 
	ble_setting_data_handler_t   data_handler;                          
} ble_setting_t;

static uint8_t queued_write_buffer[QUEUED_WRITE_BUFFER_SIZE];
static ble_user_mem_block_t mem_block;
uint32_t ble_setting_init(ble_setting_t * p_setting, const ble_setting_init_t * p_setting_init);
void ble_setting_on_ble_evt(ble_setting_t * p_setting, ble_evt_t * p_ble_evt);
void setting_data_handler(ble_setting_t * p_setting, uint8_t * p_data, uint16_t length);
uint32_t setting_notification_send(ble_setting_t * p_setting, uint8_t *p_string, uint16_t length);
#endif // BLE_SETTING_H__







